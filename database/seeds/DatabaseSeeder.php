<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Role;
use App\Models\Service;
use App\Models\LocationType;
use App\Models\Location;
use App\Models\Category;
use App\Models\Manufacturer;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        $this->call(AccessSeeder::class);
        $this->call(DataSeeder::class);

        Model::reguard();
    }

}

class AccessSeeder extends Seeder {

    public function run() {

        $superAdmin = new Role();
        $superAdmin->name = 'super-admin';
        $superAdmin->display_name = 'Super Admin';
        $superAdmin->description = 'Users with this role can access user management section.';
        $superAdmin->save();

        $admin = new Role();
        $admin->name = 'admin';
        $admin->display_name = 'Admin';
        $admin->description = 'Users with this role can access setup section.';
        $admin->save();

        $agent = new Role();
        $agent->name = 'agent';
        $agent->display_name = 'Agent';
        $agent->description = 'Users with this role can view their own job cards and create new.';
        $agent->save();

        $driver = new Role();
        $driver->name = 'driver';
        $driver->display_name = 'Driver';
        $driver->description = 'Users with this role can view their own job cards.';
        $driver->save();

        $user = new User();
        $user->name = 'Zeeshan Mehtab';
        $user->email = 'zeeshan.mehtab1@gmail.com';
        $user->password = Hash::make('123456');
        $user->confirmed = 1;
        $user->save();

        $user->attachRole($superAdmin);
        $user->attachRole($admin);

        $user2 = new User();
        $user2->name = 'Test Agent';
        $user2->email = 'agent@localhost.com';
        $user2->password = Hash::make('123456');
        $user2->confirmed = 1;
        $user2->commission = 10;
        $user2->save();

        $user2->attachRole($agent);

        $user3 = new User();
        $user3->name = 'Test Driver';
        $user3->email = 'driver@localhost.com';
        $user3->password = Hash::make('123456');
        $user3->confirmed = 1;
        $user3->save();

        $user3->attachRole($driver);
    }

}

class DataSeeder extends Seeder {

    public function run() {
        $service = new Service();
        $service->title = "Desert Safari";
        $service->rate = 120;
        $service->save();

        $service2 = new Service();
        $service2->title = "Dhow Cruise Dinner";
        $service2->rate = 100;
        $service2->save();

        $loc_type = new LocationType();
        $loc_type->title = "Camp";
        $loc_type->save();

        $loc_type2 = new LocationType();
        $loc_type2->title = "Hotel";
        $loc_type2->save();

        $loc = new Location();
        $loc->title = "Awir Camp";
        $loc->location_type_id = 1;
        $loc->save();

        $loc2 = new Location();
        $loc2->title = "Burjman Hotel";
        $loc2->location_type_id = 2;
        $loc2->save();

        $category = new Category();
        $category->name = "4 x 4";
        $category->slug = "4-4";
        $category->main_category = "transport";
        $category->save();

        $category2 = new Category();
        $category2->name = "Saloon";
        $category2->slug = "saloon";
        $category2->main_category = "transport";
        $category2->save();

        $man = new Manufacturer();
        $man->name = "Toyota";
        $man->save();

        $man2 = new Manufacturer();
        $man2->name = "Nissan";
        $man2->save();
    }

}
