<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->enum('payment_on', array('advance', 'site', 'credit'))->default('site');
            $table->enum('payment_status', array('pending', 'received'))->default('pending');
            $table->smallInteger('received_amount')->unsigned()->default(0);
            $table->string('payment_detail', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jobs', function (Blueprint $table) {
            $table->dropColumn('payment_on');
            $table->dropColumn('payment_status');
            $table->dropColumn('received_amount');
            $table->dropColumn('payment_detail');
        });
    }
}
