<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('jobs', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');

            $table->smallInteger('no_of_person')->unsigned()->default(1);
            $table->smallInteger('no_of_person_charged')->unsigned()->default(1);
            
            $table->string('customer_name', 100)->nullable();
            $table->string('customer_contact', 100)->nullable();
            $table->string('vehicle_plate_no', 100)->nullable();
            
            $table->text('comments')->nullable();

            $table->smallInteger('rate')->unsigned()->default(0);
            $table->smallInteger('total_charges')->unsigned()->default(0);
            $table->smallInteger('commission')->unsigned()->default(0);
            $table->smallInteger('total_commission')->unsigned()->default(0);
            
            $table->date('pickup_date')->nullable();
            $table->string('pickup_time', 100)->nullable();
            $table->enum('status', array('initial', 'pending', 'cancelled', 'approved', 'finished'))->default('initial');

            $table->integer('pickup_location_id')->unsigned()->index()->nullable();
            $table->foreign('pickup_location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->string('pickup_address', 255)->nullable();
            
            $table->integer('dropoff_location_id')->unsigned()->index()->nullable();
            $table->foreign('dropoff_location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->string('dropoff_address', 255)->nullable();
            
            $table->integer('agent_id')->unsigned()->index()->nullable();
            $table->foreign('agent_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('driver_id')->unsigned()->index()->nullable();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');

            $table->integer('service_id')->unsigned()->index()->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('jobs');
    }

}
