<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserHasRegistered' => [
            'App\Listeners\UserHasRegisteredSendVerification',
        ],
        'App\Events\UserHasVerified' => [
            'App\Listeners\UserHasVerifiedSendNotification',
        ],
        'App\Events\JobWasCreated' => [
            'App\Listeners\JobWasCreatedSetFields',
            'App\Listeners\JobWasCreatedSendNotification',
        ],
        'App\Events\JobWasUpdated' => [
            'App\Listeners\JobWasUpdatedSendNotification',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
