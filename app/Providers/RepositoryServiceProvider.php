<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register Repositories.
     *
     * This service provider is a place to register different repositories.
     *
     * @return void
     */
    public function register() {
       
        
        $this->app->bind(
                        'App\Repositories\Contracts\VehicleRepositoryInterface', 
                        'App\Repositories\DbVehicleRepository'
                    );
        $this->app->bind(
                        'App\Repositories\Contracts\JobRepositoryInterface', 
                        'App\Repositories\DbJobRepository'
                    );
        $this->app->bind(
                        'App\Repositories\Contracts\PageRepositoryInterface', 
                        'App\Repositories\DbPageRepository'
                    );
        $this->app->bind(
                        'App\Repositories\Contracts\SettingRepositoryInterface', 
                        'App\Repositories\DbSettingRepository'
                    );
    }

}
