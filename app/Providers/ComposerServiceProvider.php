<?php

namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    public function boot() {

        View::composer('*', 'App\Http\ViewComposers\VehiclesComposer');
    }

    public function register() {
        
    }

//put your code here
}
