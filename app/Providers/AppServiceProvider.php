<?php

namespace App\Providers;
use App\Models\Job;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        
        Job::saving(function ($job) {
            if (empty($job->pickup_location_id)) {
                $job->pickup_location_id = null;
            }
            if (empty($job->dropoff_location_id)) {
                $job->dropoff_location_id = null;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
