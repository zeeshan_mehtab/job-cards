<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class JobWasUpdated extends Event {

    use SerializesModels;

    public $job;
    public $oldJob;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($job, $oldJob) {
        $this->job = $job;
        $this->oldJob = $oldJob;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn() {
        return [];
    }

}
