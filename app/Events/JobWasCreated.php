<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class JobWasCreated extends Event {

    use SerializesModels;

    public $job;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($job) {
        $this->job = $job;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn() {
        return [];
    }

}
