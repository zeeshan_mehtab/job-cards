<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Repositories\Contracts\DeliveryRepositoryInterface;
use Carbon\Carbon;
use App\Hanas\Handlers\CartHandlerFactory;
use Illuminate\Support\Facades\Mail;

class MyAccountController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | My Account Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated.
      |
     */

    protected $order;
    protected $delivery;

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('front.my-account');
    }
    
    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function my_orders() {
        
        $user = Auth::user();
        $boxes = $this->order->buildReportQuery()->filterByAll(['customer' => $user->customer->id, 'status_in' => ['active', 'pending_payment'] ,'type' => 'Box'])
                            ->orderBy('created_at', 'DESC')
                            ->all();
        
        $cleanses = $this->order->buildReportQuery()->filterByAll(['customer' => $user->customer->id, 'type' => 'Cleanse'])
                            ->orderBy('created_at', 'DESC')
                            ->all();
        $today = Carbon::now();
        return view('front.my_account.orders')
                ->with('allowed_cancel_date', $today->addDays(3))
                ->with('my_boxes', $boxes)
                ->with('my_cleanses', $cleanses);
    }
    
    public function cancel_subscription(Request $request) {
        
        $id = $request->get('order_id');
        $order = $this->order->find($id);
        
        $user = Auth::user();
        if($order->customer->id != $user->customer->id) {
            return redirect("my-account/orders")->with(['error' => 'You do not have permission to cancel this subscription.']);
        }
        
        $order->update(['status' => 'cancelled']);
        
        return redirect("my-account/orders")->with(['success' => 'Your subscription has been cancelled.']);
    }
    
    public function manage_deliveries($id) {
        
        $order = $this->order->find($id);

        if($order->programme->group->group_type->name != 'Box') {
            return redirect("my-account/orders")->with(['error' => 'Deliveries can only be managed for box subscriptions.']);
        }
        
        $user = Auth::user();
        if($order->customer->id != $user->customer->id) {
            return redirect("my-account/orders")->with(['error' => 'You do not have permission to cancel this subscription.']);
        }
        
        $deliveries = $this->delivery->editableDeliveries($id);
        
        return view('front.my_account.manage_deliveries')
                ->with('order', $order)
                ->with('deliveries', $deliveries);
    }
    
    public function view_deliveries($id) {
        
        $order = $this->order->find($id);
        
        $user = Auth::user();
        if($order->customer->id != $user->customer->id) {
            return redirect("my-account/orders")->with(['error' => 'You do not have permission to cancel this subscription.']);
        }
        
        $filters = ['order' => $id];
        $deliveries = $this->delivery->orderBy('delivery_date', 'desc')->filterByAll($filters)->paginate($this->pagesize)->appends($filters);

        return view('front.my_account.view_deliveries')
                ->with('order', $order)
                ->with('deliveries', $deliveries);
    }
    
    public function update_delivery(Request $request) {
        
        $id = $request->get('delivery_id');
        $delivery = $this->delivery->find($id);
        
        $user = Auth::user();
        if($delivery->order->customer->id != $user->customer->id) {
            return redirect("my-account/orders")->with(['error' => 'You do not have permission to cancel this subscription.']);
        }
        
        $delivery_date = $request->get('delivery_date');

        if($delivery_date > $delivery->order->next_payment_date) {
            return redirect("my-account/orders/manage-deliveries/".$delivery->order->id)->with(['error' => 'Delivery date should be within the order cycle.']);
        }
        
        $today = Carbon::now();
        
        if($delivery_date < $today->addDays(3)) {
            return redirect("my-account/orders/manage-deliveries/".$delivery->order->id)->with(['error' => 'Delivery date has to be at least 2 days in future.']);
        }
        
        $delivery->update(['delivery_date' => $request->get('delivery_date')]);
        
        return redirect("my-account/orders/manage-deliveries/".$delivery->order->id)->with(['success' => 'Delivery date has been updated.']);
    }
    
    public function update_start_date(Request $request) {
        
        $id = $request->get('order_id');
        $order = $this->order->find($id);

        $user = Auth::user();
        if($order->customer->id != $user->customer->id) {
            return redirect("my-account/orders")->with(['error' => 'You do not have permission to update this subscription.']);
        }
        
        $minimum_date = Carbon::now()->addDays(3);
        
        if($order->start_date < $minimum_date) {
            return redirect("my-account/orders")->with(['error' => 'This order can not be cancelled now.']);
        }
        
        $start_date = $request->get('start_date');
        
        if($start_date < $minimum_date) {
            return redirect("my-account/orders")->with(['error' => 'Start date has to be at least 2 days in future.']);
        }
        
        if ($order->programme->group->group_type->name == 'Cleanse') {
            $order->update(['start_date' => $request->get('start_date')]);
            $order->deliveries()->forceDelete();
            $handler = CartHandlerFactory::getHandler('cleanse');
            $handler->handleDelivery($order->start_date, $order->programme->duration, $order->programme->deliverables_per_cycle, $order->id);
            
            Mail::send('emails.customer.order-received', array('order' => $order), function($message) use ($order) {
                $message->to($order->customer->user->email, $order->customer->user->name)->subject('Your Order Received');
            });
        }
        
        return redirect("my-account/orders/view-deliveries/".$order->id)->with(['success' => 'Start date has been updated. Please review new delivery plan.']);
    }

}
