<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\Front\ContactRequest;
use App\Events\ContactUsFormWasSubmitted;
use Illuminate\Support\Facades\Event;

class FormsController extends Controller {

    public function getContactUs() {
        return view("front.forms.contact");
    }

    public function postContactUs(ContactRequest $request) {


        Event::fire(new ContactUsFormWasSubmitted($request->only('name', 'phone', 'email', 'detail')));

        return redirect("contact-us")
                        ->with(['success' => 'Thank you for contacting us. We will get back to you soon.']);
    }

}
