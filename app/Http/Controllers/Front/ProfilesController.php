<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileRequest;
use Illuminate\Support\Facades\Auth;

class ProfilesController extends Controller {

    /**
     * Show the form for editing the profile.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $user = Auth::user();

        return view("front.profile.edit")
                        ->with('user', $user);
    }

    /**
     * Update the specified profile in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ProfileRequest $request) {

        $user = Auth::user();
        
        $user->update($request->only(['name', 'email', 'phone']));
        //$user->customer->update($request->only(['first_name', 'last_name', 'phone', 'dob', 'gender']));
        
        return redirect("my-account");
    }

}
