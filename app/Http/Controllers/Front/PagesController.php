<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\PageRepositoryInterface;

class PagesController extends Controller {

    protected $page;

    public function __construct(PageRepositoryInterface $page) {
        $this->page = $page;
    }

    public function page($slug) {

        $page = $this->page->findBySlug($slug);
        if(!$page) {
            return redirect("/");
        }
        return view("front.cms.page")->with('page', $page);
    }

}
