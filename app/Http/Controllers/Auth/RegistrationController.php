<?php

/*
 * This file is part of News Stand Project.
 * Copyright (c) 2015 Zeeshan Mehtab.
 * This file is open source and can be used and redistributed
 */

/**
 * Description of RegistrationController
 *
 * @author Zeeshan
 */

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Events\UserHasVerified;
use Illuminate\Support\Facades\Event;

class RegistrationController extends Controller {

    public function confirm($confirmation_code) {
        if (!$confirmation_code) {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('confirmed', 0)
                ->where('confirmation_code', $confirmation_code)
                ->first();

        if (!$user) {
            return redirect("auth/login")->with(['error' => 'Invalid verification code']);
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();
        
        Event::fire(new UserHasVerified($user));
        
        return redirect("auth/login")->with(['success' => 'You have successfully verified your account.']);
    }

}
