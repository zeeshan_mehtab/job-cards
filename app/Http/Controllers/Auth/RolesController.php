<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Http\Requests\RoleRequest;
use Illuminate\Http\Request;

class RolesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $roles = Role::oldest("name")->paginate($this->pagesize);
        return view("admin.roles.index")->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view("admin.roles.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(RoleRequest $request) {

        Role::create($request->all());
        return redirect("admin/auth/roles")->with(['success' => 'New Role created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $role = Role::findOrFail($id);
        return view("admin.roles.show")->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $role = Role::findOrFail($id);

        return view("admin.roles.edit")
                        ->with('role', $role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, RoleRequest $request) {

        $role = Role::findOrFail($id);
        $role->update($request->all());
        return redirect("admin/auth/roles")->with(['success' => 'Role updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $role = Role::findOrFail($id);
        $role->delete();
        return redirect("admin/auth/roles")->with(['success' => 'Role deleted successfully.']);
    }

}
