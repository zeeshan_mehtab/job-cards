<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\UserPermissionRequest;
use App\Models\User;
use App\Customer;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $users = User::orderBy('name', 'asc')->paginate($this->pagesize);
        return view("admin.users.index")->with('users', $users);
    }

    public function customers() {

        $customers = Customer::allCustomers()->paginate($this->pagesize);
        return view("admin.users.customers")->with('customers', $customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        $roles = Role::lists('name', 'id')->all();
        $permissions = Permission::lists('name', 'id')->all();
        return view("admin.users.create")
                        ->with('permissions', $permissions)
                        ->with('roles', $roles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(UserRequest $request) {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $user->confirmed = 1;
        $user->save();
        $user->roles()->sync($request->input("role_list"));
        return redirect("admin/auth/users")->with(['success' => 'New User created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {
        $user = User::findOrFail($id);
        $permissions = Permission::lists('name', 'id')->all();
        return view("admin.users.show")
                        ->with('permissions', $permissions)
                        ->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
        $user = User::findOrFail($id);
        $roles = Role::lists('name', 'id')->all();
        $permissions = Permission::lists('name', 'id')->all();
        $selected_roles = $user->roles->lists('id')->all();

        return view("admin.users.edit")
                        ->with('user', $user)
                        ->with('permissions', $permissions)
                        ->with('roles', $roles)
                        ->with('selected_roles', $selected_roles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, UserUpdateRequest $request) {
        $user = User::findOrFail($id);
        $input = $request->all();
        if (empty($input['password'])) {
            unset($input['password']);
        } else {
            $input['password'] = bcrypt($input['password']);
        }
        $user->update($input);
        $user->roles()->sync($request->input("role_list"));
        return redirect("admin/auth/users")->with(['success' => 'User updated successfully.']);
    }

    /*
     * Add a permission to the user permission pivot table
     */

    public function permission($id, UserPermissionRequest $request) {
        $user = User::findOrFail($id);
        $user->permissions()->attach($request->get("permission_id"), array('quantity' => $request->get("quantity")));
        return redirect("admin/auth/users/$id");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $user = User::findOrFail($id);

        $customer = $user->customer;
        $customer->addresses()->delete();
        $customer->cards()->delete();
        $customer->delete();

        $user->delete();
        return redirect("admin/auth/users")->with(['success' => 'User deleted successfully.']);
    }

}
