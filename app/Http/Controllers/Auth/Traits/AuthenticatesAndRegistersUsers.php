<?php

/*
 * This file is part of News Stand Project.
 * Copyright (c) 2015 Zeeshan Mehtab.
 * This file is open source and can be used and redistributed
 */

namespace App\Http\Controllers\Auth\Traits;

trait AuthenticatesAndRegistersUsers {

    use AuthenticatesUsers,
        RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
    }
}
