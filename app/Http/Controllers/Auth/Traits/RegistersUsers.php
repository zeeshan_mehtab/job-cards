<?php

/*
 * This file is part of News Stand Project.
 * Copyright (c) 2015 Zeeshan Mehtab.
 * This file is open source and can be used and redistributed
 */

namespace App\Http\Controllers\Auth\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

trait RegistersUsers {

    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister() {
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request) {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                    $request, $validator
            );
        }
        
        $this->create($request->all());

        return redirect("auth/login")->with(['success' => 'Thank you for registration. Please verify your email address and then login to your account.']);
    }

}
