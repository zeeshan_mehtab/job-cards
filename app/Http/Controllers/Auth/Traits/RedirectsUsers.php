<?php

namespace App\Http\Controllers\Auth\Traits;

use Illuminate\Support\Facades\Auth;

trait RedirectsUsers
{
    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }
        
        $role_based_redirect = "/home";
        $user = Auth::user();
        if($user) {
            if($user->hasRole('super-admin') || $user->hasRole('admin') ) {
                $role_based_redirect = "/admin";
            } else if($user->hasRole('agent') ) {
                $role_based_redirect = "/admin/listing/agent/jobs";
            } else if($user->hasRole('driver') ) {
                $role_based_redirect = "/admin/listing/driver/jobs";
            } else {
                $role_based_redirect = "/home";
            }
        }
        return property_exists($this, 'redirectTo') ? $this->redirectTo : $role_based_redirect;
    }
}
