<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Http\Requests\VehicleRequest;
use Illuminate\Support\Facades\Input;
use App\Repositories\Contracts\VehicleRepositoryInterface;
use App\Http\Requests\Import\VehicleImport;

class VehiclesController extends Controller {

    protected $vehicle;

    public function __construct(VehicleRepositoryInterface $repository) {
        $this->vehicle = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $filters = Input::all();
        $vehicles = $this->vehicle->filterByAll($filters)->paginate($this->pagesize)->appends($filters);
        return view("admin.vehicles.index")
                        ->with('filters', $filters)
                        ->with('vehicles', $vehicles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        $manufacturers = Manufacturer::lists('name', 'id');

        $categories = Category::lists('name', 'id');

        return view("admin.vehicles.create")
                        ->with('categories', $categories)
                        ->with('manufacturers', $manufacturers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(VehicleRequest $request) {

        $vehicle = Vehicle::create($request->all());
        return redirect("admin/listing/vehicles/$vehicle->id")->with(['success' => 'New Vehicle created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $vehicle = Vehicle::findOrFail($id);
        return view("admin.vehicles.show")
                        ->with('vehicle', $vehicle);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $vehicle = Vehicle::findOrFail($id);
        $manufacturers = Manufacturer::lists('name', 'id');
        $categories = Category::lists('name', 'id');

        return view("admin.vehicles.edit")
                        ->with('categories', $categories)
                        ->with('vehicle', $vehicle)
                        ->with('manufacturers', $manufacturers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, VehicleRequest $request) {

        $vehicle = Vehicle::findOrFail($id);
        $vehicle->update($request->all());
        return redirect("admin/listing/vehicles")->with(['success' => 'Vehicle updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $vehicle = Vehicle::findOrFail($id);
        $vehicle->delete();
        return redirect("admin/listing/vehicles")->with(['success' => 'Vehicle deleted successfully.']);
    }

    public function get_import() {
        return view("admin.vehicles.import");
    }

    public function post_import(VehicleImport $import) {

        $import->handleImport();

        return redirect("admin/listing/vehicles")->with(['success' => 'File imported successfully.']);
    }

}
