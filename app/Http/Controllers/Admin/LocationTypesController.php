<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LocationType;
use App\Http\Requests\LocationTypeRequest;

class LocationTypesController extends Controller {

    public function index() {

        $location_types = LocationType::latest("title")->paginate($this->pagesize);
        return view("admin.location_types.index")->with('location_types', $location_types);
    }

    public function create() {
        return view("admin.location_types.create");
    }

    public function store(LocationTypeRequest $request) {

        LocationType::create($request->all());
        return redirect("admin/setup/location_types")->with(['success' => 'New Location Type created successfully.']);
    }

    public function edit($id) {

        $location_type = LocationType::findOrFail($id);

        return view("admin.location_types.edit")
                        ->with('location_type', $location_type);
    }

    public function update($id, LocationTypeRequest $request) {

        $location_type = LocationType::findOrFail($id);
        $location_type->update($request->all());
        return redirect("admin/setup/location_types")->with(['success' => 'LocationType updated successfully.']);
    }

    public function destroy($id) {

        $location_type = LocationType::findOrFail($id);
        $location_type->delete();
        return redirect("admin/setup/location_types")->with(['success' => 'LocationType deleted successfully.']);
    }

}
