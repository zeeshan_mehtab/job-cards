<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Illuminate\Support\Facades\Input;
use App\Repositories\Contracts\JobRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AgentJobRequest;
use App\Events\JobWasCreated;
use Illuminate\Support\Facades\Event;
use App\Http\Requests\Export\JobCardExport;

class AgentsController extends Controller {

    protected $job;

    public function __construct(JobRepositoryInterface $repository) {
        $this->job = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $filters = Input::all();
        $filters['agent'] = Auth::user()->id;
        $jobs = $this->job->filterByAll($filters)->paginate($this->pagesize)->appends($filters);
        return view("admin.agents.index")
                        ->with('filters', $filters)
                        ->with('jobs', $jobs);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view("admin.agents.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(AgentJobRequest $request) {
        
        $input = $request->all();
        $input['agent_id'] = Auth::user()->id;
        $job = Job::create($input);
        if($job) {
            Event::fire(new JobWasCreated($job));
        }
        return redirect("admin/listing/agent/jobs")->with(['success' => 'New Job created successfully.']);
    }

    public function show($id) {

        $job = Job::findOrFail($id);
        return view("admin.agents.show")
                        ->with('job', $job);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $job = Job::findOrFail($id);
        if($job->status == "cancelled" || $job->status == "approved" || $job->status == "finished") {
            return redirect("admin/listing/agent/jobs")->with(['error' => 'This job can not be updated anymore. Please contact Manager.']);
        }
        return view("admin.agents.edit")
                        ->with('job', $job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, AgentJobRequest $request) {

        $job = Job::findOrFail($id);
        $job->update($request->all());
        
        if($job) {
            Event::fire(new JobWasCreated($job));
        }
        return redirect("admin/listing/agent/jobs")->with(['success' => 'Job updated successfully.']);
    }
    
    public function export(JobCardExport $export) {
        $export->handleExport();
    }

}
