<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Models\LocationType;
use App\Http\Requests\LocationRequest;

class LocationsController extends Controller {

    public function index() {

        $locations = Location::latest("title")->paginate($this->pagesize);
        return view("admin.locations.index")->with('locations', $locations);
    }

    public function create() {
        $location_types = LocationType::lists('title', 'id')->all();
        return view("admin.locations.create")
                        ->with('location_types', $location_types);
    }

    public function store(LocationRequest $request) {

        Location::create($request->all());
        return redirect("admin/listing/locations")->with(['success' => 'New Location created successfully.']);
    }

    public function edit($id) {

        $location = Location::findOrFail($id);
        $location_types = LocationType::lists('title', 'id')->all();

        return view("admin.locations.edit")
                        ->with('location', $location)
                        ->with('location_types', $location_types);
    }

    public function update($id, LocationRequest $request) {

        $location = Location::findOrFail($id);
        $location->update($request->all());
        return redirect("admin/listing/locations")->with(['success' => 'Location updated successfully.']);
    }

    public function destroy($id) {

        $location = Location::findOrFail($id);
        $location->delete();
        return redirect("admin/listing/locations")->with(['success' => 'Location deleted successfully.']);
    }

}
