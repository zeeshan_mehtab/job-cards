<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Http\Requests\ServiceRequest;

class ServicesController extends Controller {

    public function index() {

        $services = Service::latest("title")->paginate($this->pagesize);
        return view("admin.services.index")->with('services', $services);
    }

    public function create() {
        return view("admin.services.create");
    }

    public function store(ServiceRequest $request) {

        Service::create($request->all());
        return redirect("admin/setup/services")->with(['success' => 'New Service created successfully.']);
    }

    public function edit($id) {

        $service = Service::findOrFail($id);

        return view("admin.services.edit")
                        ->with('service', $service);
    }

    public function update($id, ServiceRequest $request) {

        $service = Service::findOrFail($id);
        $service->update($request->all());
        return redirect("admin/setup/services")->with(['success' => 'Service updated successfully.']);
    }

    public function destroy($id) {

        $service = Service::findOrFail($id);
        $service->delete();
        return redirect("admin/setup/services")->with(['success' => 'Service deleted successfully.']);
    }

}
