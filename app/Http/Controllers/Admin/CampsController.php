<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use Illuminate\Support\Facades\Input;
use App\Repositories\Contracts\JobRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Export\JobCardExport;

class CampsController extends Controller {

    protected $job;

    public function __construct(JobRepositoryInterface $repository) {
        $this->job = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {
        $filters = Input::all();
        $filters['camp_boss'] = $user = Auth::user()->id;
        $filters['today'] = true;
        $jobs = $this->job->filterByAll($filters)->paginate($this->pagesize)->appends($filters);
        return view("admin.camp.index")
                        ->with('filters', $filters)
                        ->with('jobs', $jobs);
    }

    public function show($id) {

        $job = Job::findOrFail($id);
        return view("admin.drivers.show")
                        ->with('job', $job);
    }
    
    public function export(JobCardExport $export) {
        $export->handleExport();
    }

}
