<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Http\Requests\ManufacturerRequest;
use Illuminate\Http\Request;

class ManufacturersController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $manufacturers = Manufacturer::oldest("name")->paginate($this->pagesize);
        return view("admin.manufacturers.index")->with('manufacturers', $manufacturers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        return view("admin.manufacturers.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ManufacturerRequest $request) {

        Manufacturer::create($request->all());
        return redirect("admin/setup/manufacturers")->with(['success' => 'New Manufacturer created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $manufacturer = Manufacturer::findOrFail($id);
        $categories = Category::lists('name', 'id');
        return view("admin.manufacturers.show")
                ->with('categories', $categories)
                ->with('manufacturer', $manufacturer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $manufacturer = Manufacturer::findOrFail($id);
        return view("admin.manufacturers.edit")->with('manufacturer', $manufacturer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, ManufacturerRequest $request) {

        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->update($request->all());
        return redirect("admin/setup/manufacturers")->with(['success' => 'Manufacturer updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->delete();
        return redirect("admin/setup/manufacturers")->with(['success' => 'Manufacturer deleted successfully.']);
    }
    
    public function category($id, Request $request) {
        $manufacturer = Manufacturer::findOrFail($id);
        if($request->get("category_id")) {
            $manufacturer->categories()->attach($request->get("category_id"));
        }
        return redirect("admin/setup/manufacturers/$id#categories");
    }
    
    public function destroycategory($id, $category) {
        
        $manufacturer = Manufacturer::findOrFail($id);
        $manufacturer->categories()->detach($category);
        return redirect("admin/setup/manufacturers/$id#categories");
    }

}
