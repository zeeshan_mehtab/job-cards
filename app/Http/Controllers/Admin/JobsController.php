<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Http\Requests\JobRequest;
use App\Http\Requests\LogRequest;
use Illuminate\Support\Facades\Input;
use App\Repositories\Contracts\JobRepositoryInterface;
use App\Http\Requests\Import\JobImport;
use Illuminate\Support\Facades\Event;
use App\Http\Requests\Export\JobCardExport;
use App\Events\JobWasUpdated;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;

class JobsController extends Controller {

    protected $job;

    public function __construct(JobRepositoryInterface $repository) {
        $this->job = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request) {
        $filters = Input::all();
        
        if(empty($filters) && $request->session()->has('filters')) {
			//$filters = $request->session()->get('filters');
		} else if(!empty($filters)) {
			//$request->session()->put('filters', $filters);
		}
		
        $jobs = $this->job->filterByAll($filters)->paginate($this->pagesize)->appends($filters);
        return view("admin.jobs.index")
                        ->with('filters', $filters)
                        ->with('jobs', $jobs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {
        return view("admin.jobs.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(JobRequest $request) {
        $job = Job::create($request->all());
        return redirect("admin/listing/jobs")->with(['success' => 'New Job created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $job = Job::findOrFail($id);
        return view("admin.jobs.show")
                        ->with('job', $job);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $job = Job::findOrFail($id);
        $user = Auth::user();
        
        if($job->status == "approved" && !$user->hasRole('super-admin')) {
            return redirect("admin/listing/jobs")->with(['error' => 'This job is in approved state and can not be updated anymore. Please contact Admin.']);
        }
        
        return view("admin.jobs.edit")
                        ->with('job', $job);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, JobRequest $request) {
        
        $job = Job::findOrFail($id);
        $oldJob = Job::findOrFail($id);
        
        $job->update($request->all());
        
        if($job) {
            Event::fire(new JobWasUpdated($job, $oldJob));
        }
        
        if ($request->get('action') == 'save') {
			return redirect("admin/listing/jobs/$id/edit")->with(['success' => 'Job updated successfully.']);
		} elseif ($request->get('action') == 'save_and_close') {
			$pickupDateTime = explode(" ", $request->get('pickup_date'));
			$pickupDate = $pickupDateTime[0];
			return redirect("admin/listing/jobs?from=".date('Y-m-d')."&to=".date('Y-m-d'))->with(['success' => 'Job updated successfully.']);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {

        $job = Job::findOrFail($id);
        $job->delete();
        return redirect("admin/listing/jobs")->with(['success' => 'Job deleted successfully.']);
    }
    
    public function export(JobCardExport $export) {
        $export->handleExport();
    }

    public function get_import() {
        return view("admin.jobs.import");
    }

    public function post_import(JobImport $import) {

        $import->handleImport();

        return redirect("admin/listing/jobs")->with(['success' => 'File imported successfully.']);
    }

}
