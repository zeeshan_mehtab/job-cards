<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use App\Repositories\Contracts\JobRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class ReportsController extends Controller {

    protected $job;

    public function __construct(JobRepositoryInterface $job) {
        $this->job = $job;
        $this->middleware('admin');
    }

    public function getTotalsPerOfficer() {
        $user = Auth::user();
        if (!$user->hasRole('admin') && !$user->hasRole('super-admin') && !$user->hasRole('account')) {
            exit();
        }
        
        $from = Input::get('from') ? Input::get('from') : date("Y-m-d");
        $to = Input::get('to') ? Input::get('to') : date("Y-m-d");
        $input = [
          'from' => $from,  
          'to' => $to,  
        ];
        
        $jobs = $this->job->buildTotalsQuery()
                ->filterByAll($input)
                ->all();
        
        return view("admin.dashboard.totals_per_officer.index")
                        ->with('input', Input::all())
                        ->with('jobs', $jobs);
    }
    
    public function getCommissionPerAgent() {
        $user = Auth::user();
        if (!$user->hasRole('admin') && !$user->hasRole('super-admin') && !$user->hasRole('account')) {
            exit();
        }
        
        $from = Input::get('from') ? Input::get('from') : date("Y-m-d");
        $to = Input::get('to') ? Input::get('to') : date("Y-m-d");
        $input = [
          'from' => $from,  
          'to' => $to,  
        ];
        
        $jobs = $this->job->buildCommissionQuery()
                ->filterByAll($input)
                ->all();
        
        return view("admin.dashboard.commission_per_agent.index")
                        ->with('input', Input::all())
                        ->with('jobs', $jobs);
    }
    
    public function getJobsPerDay() {
        
        $user = Auth::user();
        
        $query = DB::table('jobs')
                ->select(DB::raw('DATE(`pickup_date`) as `date`'), DB::raw('count(*) as total'))
                ->where('jobs.created_at', '>', new \DateTime('-30 days'));
        if ($user->hasRole('agent')) {
            $query = $query->where('jobs.agent_id', '=', $user->id);
        } else if ($user->hasRole('driver')) {
            $query = $query->where('jobs.driver_id', '=', $user->id);
        } else if ($user->hasRole('camp_boss')) {
            $query = $query->where('jobs.camp_boss_id', '=', $user->id);
        }
        
        $result = $query->groupBy('date')
                        ->get();
        return $result;
    }
    
    public function getJobsPerService() {
        
        $user = Auth::user();
        
        $query = DB::table('jobs')
                ->select('services.title as service', DB::raw('count(*) as total'))
                ->join('services', 'jobs.service_id', '=', 'services.id', 'left')
                ->where('jobs.created_at', '>', new \DateTime('-30 days'));
        if ($user->hasRole('agent')) {
            $query = $query->where('jobs.agent_id', '=', $user->id);
        } else if ($user->hasRole('driver')) {
            $query = $query->where('jobs.driver_id', '=', $user->id);
        } else if ($user->hasRole('camp_boss')) {
            $query = $query->where('jobs.camp_boss_id', '=', $user->id);
        }
        
        $result = $query->groupBy('service')
                        ->get();
        return $result;
    }

}
