<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminDashboardController extends Controller {

    public function __construct() {
        //$this->middleware('has-admin-dashboard-access');
    }

    /**
     * Display a dashboard view for the admin users
     *
     * @return Response
     */
    public function index() {
        return view("admin.dashboard.index");
    }

}
