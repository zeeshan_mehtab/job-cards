<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoriesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() {

        $categories = Category::oldest("name")->paginate($this->pagesize);
        return view("admin.categories.index")->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() {

        return view("admin.categories.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoryRequest $request) {

        Category::create($request->all());
        return redirect("admin/setup/categories")->with(['success' => 'New Category created successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id) {

        $category = Category::findOrFail($id);
        return view("admin.categories.show")->with('category', $category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {

        $category = Category::findOrFail($id);
        return view("admin.categories.edit")->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, CategoryRequest $request) {

        $category = Category::findOrFail($id);
        $category->update($request->all());
        return redirect("admin/setup/categories")->with(['success' => 'Cagegory updated successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id) {
        $category = Category::findOrFail($id);
        $category->delete();
        return redirect("admin/setup/categories")->with(['success' => 'Category deleted successfully.']);
    }

}
