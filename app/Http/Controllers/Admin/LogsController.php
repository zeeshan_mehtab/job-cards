<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Models\LogType;
use App\Http\Requests\LogRequest;

class LogsController extends Controller {

    public function store(LogRequest $request) {

        Log::create($request->all());
        $job_id = $request->get("job_id");
        return redirect("admin/listing/jobs/$job_id#logs")->with(['success' => 'New Log created successfully.']);
    }

    public function destroy($id) {

        $location = Log::findOrFail($id);
        $location->delete();
        return redirect("admin/listing/locations")->with(['success' => 'Log deleted successfully.']);
    }

}
