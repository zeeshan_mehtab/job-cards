<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Http\Requests\SettingRequest;

class SettingsController extends Controller {

    public function index() {

        $settings = Setting::oldest("key")->paginate($this->pagesize);
        return view("admin.settings.index")->with('settings', $settings);
    }

    public function create() {
        return view("admin.settings.create");
    }

    public function store(SettingRequest $request) {

        Setting::create($request->all());
        return redirect("admin/setup/settings")->with(['success' => 'New Setting created successfully.']);
    }

    public function edit($id) {

        $setting = Setting::findOrFail($id);

        return view("admin.settings.edit")
                        ->with('setting', $setting);
    }

    public function update($id, SettingRequest $request) {

        $setting = Setting::findOrFail($id);
        $setting->update($request->all());
        return redirect("admin/setup/settings")->with(['success' => 'Setting updated successfully.']);
    }

    public function destroy($id) {

        $setting = Setting::findOrFail($id);
        $setting->delete();
        return redirect("admin/setup/settings")->with(['success' => 'Setting deleted successfully.']);
    }

}
