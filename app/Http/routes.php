<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'Front\WelcomeController@index');
Route::get('home', 'Front\WelcomeController@index');

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::controllers([
    'password' => 'Auth\PasswordController',
]);

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\RegistrationController@confirm'
]);


Route::group(array('prefix' => 'my-account', 'middleware' => 'auth'), function() {
    Route::get('', 'Front\MyAccountController@index');
    Route::get('orders', 'Front\MyAccountController@my_bids');
    Route::get('profile/edit', 'Front\ProfilesController@edit');
    Route::put('profile', 'Front\ProfilesController@update');
});

Route::group(['prefix' => 'admin', 'middleware' => ['backend']], function() {

    Route::get('', 'Admin\AdminDashboardController@index');

    Route::group(array('prefix' => 'setup', 'middleware' => 'admin'), function() {
        Route::resource('settings', 'Admin\SettingsController');
        Route::resource('location_types', 'Admin\LocationTypesController');
        Route::resource('services', 'Admin\ServicesController');
        Route::resource('categories', 'Admin\CategoriesController');
        Route::resource('manufacturers', 'Admin\ManufacturersController');
    });

    /*
     * Below routes are protected by admin middleware and thus can only be
     * accessed by users who have admin role assigned to them.
     */

    Route::group(array('prefix' => 'cms', 'middleware' => 'admin'), function() {
        //Route::resource('pages', 'PagesController');
    });

    /*
     * Below routes are protected by admin middleware and thus can only be
     * accessed by users who have admin role assigned to them.
     */
    
    Route::group(array('prefix' => 'listing', 'middleware' => 'admin'), function() {
        Route::get('jobs/export', 'Admin\JobsController@export');
        Route::resource('jobs', 'Admin\JobsController');
        Route::resource('logs', 'Admin\LogsController');
        Route::resource('locations', 'Admin\LocationsController');
        Route::resource('vehicles', 'Admin\VehiclesController');
    });
    
    Route::group(array('prefix' => 'listing', 'middleware' => 'admin'), function() {
        Route::resource('jobs', 'Admin\JobsController');
        Route::resource('locations', 'Admin\LocationsController');
        Route::resource('vehicles', 'Admin\VehiclesController');
    });
    
    Route::group(array('prefix' => 'listing/driver', 'middleware' => 'driver'), function() {
        Route::get('jobs/export', 'Admin\DriversController@export');
        Route::resource('jobs', 'Admin\DriversController');
    });
    
    Route::group(array('prefix' => 'listing/account', 'middleware' => 'account'), function() {
        Route::get('jobs/export', 'Admin\AccountsController@export');
        Route::resource('jobs', 'Admin\AccountsController');
    });
    
    Route::group(array('prefix' => 'listing/agent', 'middleware' => 'agent'), function() {
        Route::get('jobs/export', 'Admin\AgentsController@export');
        Route::resource('jobs', 'Admin\AgentsController');
    });
    
    Route::group(array('prefix' => 'listing/camp', 'middleware' => 'camp'), function() {
        Route::get('jobs/export', 'Admin\CampsController@export');
        Route::resource('jobs', 'Admin\CampsController');
    });

    /*
     * Below routes are protected by super-admin middleware and thus can only be
     * accessed by users who have super-admin role assigned to them.
     */
    Route::group(array('prefix' => 'auth', 'middleware' => 'super'), function() {

        Route::get('', 'Auth\UsersController@index');
        Route::get('customers', 'Auth\UsersController@customers');
        Route::resource('users', 'Auth\UsersController');
        Route::resource('roles', 'Auth\RolesController');
    });

    /*
     * Below routes are protected by super admin middleware and thus can only be
     * accessed by users who have super admin role assigned to them.
     */
    Route::group(array('prefix' => 'reports'), function() {
        
        Route::get('{report?}', function($report = 'index') {
            $controllerClass = 'App\\Http\\Controllers\\Admin\\ReportsController';
            $action = studly_case($report); // optional, converts foo-bar into FooBar for example
            $methodName = 'get' . $action; // this step depends on how your actions are called (get... / ...Action)
            $controller = App::make($controllerClass);
            return $controller->callAction($methodName, array());
        });
    });
});
