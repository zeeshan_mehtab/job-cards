<?php

namespace App\Http\Requests\Export;

use Maatwebsite\Excel\Files\NewExcelFile;

class JobCardExport extends NewExcelFile {

    public function getFilename() {
        return "List of Job Cards";
    }

}
