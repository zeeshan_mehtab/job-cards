<?php

namespace App\Http\Requests\Export;

use Maatwebsite\Excel\Files\ExportHandler;
use App\Repositories\Contracts\JobRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobCardExportHandler implements ExportHandler {

    protected $job;
    protected $request;

    public function __construct(JobRepositoryInterface $job, Request $request) {
        $this->job = $job;
        $this->request = $request;
    }

    public function handle($file) {

        $input = $this->request->all();
        $user = Auth::user();
        if ($user->hasRole('agent')) {
            $input['agent'] = $user->id;
        } else if ($user->hasRole('driver')) {
            $input['driver'] = $user->id;
        } else if ($user->hasRole('account')) {
            $input['current'] = true;
        }
        
        if($user->hasRole('agent') || $user->hasRole('driver')) {
            $jobs = $this->job->buildBasicReportQuery()->filterByAll($input)->orderBy("jobs.created_at", "desc")->all()->toArray();
        } else {
            $jobs = $this->job->buildReportQuery()->filterByAll($input)->orderBy("jobs.created_at", "desc")->all()->toArray();
        }
        
        return $file->sheet('deliveries', function($sheet) use($jobs) {

                    $sheet->appendRow(array_keys($jobs[0]));
                    foreach ($jobs as $job) {
                        $sheet->appendRow($job);
                    }
                })->export('xls');
    }

}
