<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LogRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {

        return [
            'comments' => 'required',
            'job_id' => 'required',
        ];
    }

}
