<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AgentJobRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        return [
            'customer_name' => 'required|min:3',
            'customer_contact' => 'required|min:3',
            'service_id' => 'required|numeric',
            'no_of_person' => 'required|numeric',
            'no_of_person_charged' => 'required|numeric',
            'pickup_location_id' => 'required_without:pickup_address',
            'dropoff_location_id' => 'required_without:dropoff_address',
            'pickup_date' => 'required',
            'pickup_time' => 'required'
        ];
    }

}
