<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LocationRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {

        return [
            'title' => 'required',
            'location_type_id' => 'required'
        ];
    }

}
