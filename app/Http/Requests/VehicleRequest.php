<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VehicleRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {

        return [
            'identification_no' => 'required|min:3',
            'description' => 'required|min:3',
            'category_id' => 'required|numeric',
            'manufacturer_id' => 'required|numeric',
            'model_year' => 'required',
            'exterior_colour' => 'required',
            'status' => 'required'
        ];
    }

}
