<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasCampAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = Auth::user();
        if ($user && $user->hasRole('camp_boss')) {
            return $next($request);
        } else {
            return redirect('/auth/login')->with(['error' => 'Please login as camp boss user.']);
        }
    }

}
