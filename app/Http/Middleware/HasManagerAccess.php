<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasManagerAccess {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $user = Auth::user();
        if ($user && ($user->hasRole('super-admin') || $user->hasRole('admin') || $user->hasRole('manager') )) {
            return $next($request);
        } else {
            return redirect('/auth/login')->with(['error' => 'Please login as manager user.']);
        }
    }

}
