<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Models\Category;
use App\Models\Role;
use App\Models\Service;
use App\Models\Manufacturer;
use App\Models\Location;

class VehiclesComposer {

    public function __construct() {
        
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view) {
        
        $manufacturers = Manufacturer::lists('name', 'id')->toArray();
        $categories = Category::lists('name', 'id')->toArray();
        $services = Service::lists('title', 'id')->toArray();
        $locations = Location::lists('title', 'id')->toArray();
        
        $agent_role = Role::where('name', 'agent')->first();
        $agents = [];
        if($agent_role) {
            $agents = $agent_role->users()->lists('name', 'id')->toArray();
        }
        
        $driver_role = Role::where('name', 'driver')->first();
        $drivers = [];
        if($driver_role) {
            $drivers = $driver_role->users()->lists('name', 'id')->toArray();
        }
        
        $camp_boss_role = Role::where('name', 'camp_boss')->first();
        $camp_bosses = [];
        if($camp_boss_role) {
            $camp_bosses = $camp_boss_role->users()->lists('name', 'id')->toArray();
        }
        
        $view->with('all_manufacturers', $manufacturers);
        $view->with('all_categories', $categories);
        $view->with('all_services', $services);
        $view->with('all_agents', $agents);
        $view->with('all_drivers', $drivers);
        $view->with('all_camp_bosses', $camp_bosses);
        $view->with('all_locations', $locations);
        $view->with('today', date("Y-m-d"));
        
        $years = [];
        for($i = date("Y"); $i > (date("Y")-20); $i--) {
            $years[$i] = $i;
        }
        $view->with('year_options', $years);
    }

}
