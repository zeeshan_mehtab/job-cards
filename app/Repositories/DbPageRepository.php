<?php

namespace App\Repositories;

/*
 * Repository to fetch ingredients from DB
 */

use App\Repositories\Contracts\PageRepositoryInterface;
use App\Page;
use App\Repositories\DbRepository;
use Illuminate\Support\Facades\DB;

class DbPageRepository extends DbRepository implements PageRepositoryInterface {

    public function __construct() {
        $this->validFilterableFields = ['name'];
        $this->query = Page::query();
    }

    public function find($id) {
        return Page::findOrFail($id);
    }

    public function all() {
        return $this->applyFiltersToQuery($this->query())->get();
    }

    public function paginate($pagesize) {
        return $this->applyFiltersToQuery($this->query())->paginate($pagesize);
    }

    public function findBySlug($slug) {
        return $this->query()->where('slug', $slug)->first();
    }

    public function getList($group) {
        $query = DB::table('pages');
        return $query->select('pages.id', 'pages.name', 'pages.title', 'pages.slug')
                ->where('group', '=', $group)
                ->get();
    }

    public function refresh() {
        
    }

}
