<?php

namespace App\Repositories;

/*
 * Repository to fetch ingredients from DB
 */

use App\Repositories\Contracts\JobRepositoryInterface;
use App\Models\Job;
use App\Repositories\DbRepository;
use Illuminate\Support\Facades\DB;

class DbJobRepository extends DbRepository implements JobRepositoryInterface {

    public function __construct() {
        $this->validFilterableFields = ['from', 'to', 'today' ,'camp_boss', 'agent', 'driver', 'pickup_location', 'dropoff_location', 'pickup_date', 'servie', 'customer_name','driver_name', 'status', 'payment_status', 'current'];
        $this->query = Job::query()
                ->select('jobs.*', 'services.title as service', 'pickup_location.title as pickup_location', 'dropoff_location.title as dropoff_location', 'drivers.name as driver_name')
                ->join('services', 'jobs.service_id', '=', 'services.id', 'left')
                ->join('locations as pickup_location', 'jobs.pickup_location_id', '=', 'pickup_location.id', 'left')
                ->join('locations as dropoff_location', 'jobs.dropoff_location_id', '=', 'dropoff_location.id', 'left')
                ->join('users as agents', 'jobs.agent_id', '=', 'agents.id', 'left')
                ->join('users as drivers', 'jobs.driver_id', '=', 'drivers.id', 'left')
                ->orderby("created_at", "desc");
    }
    
    public function buildBasicReportQuery() {

        $this->query = Job::query()
                ->select('jobs.id as id as job_id', 'jobs.customer_name', 'jobs.customer_contact', 
                        'services.title as service', 'jobs.no_of_person',
                        'pickup_location.title as pickup_location', 'pickup_address', 'dropoff_location.title as dropoff_location', 'dropoff_address',
                        'jobs.pickup_date', 'jobs.pickup_time', 'jobs.comments',
                        'agents.name as agent', 'drivers.name as driver',
                        'jobs.status'
                        
                )
                ->join('services', 'jobs.service_id', '=', 'services.id', 'left')
                ->join('locations as pickup_location', 'jobs.pickup_location_id', '=', 'pickup_location.id', 'left')
                ->join('locations as dropoff_location', 'jobs.dropoff_location_id', '=', 'dropoff_location.id', 'left')
                ->join('users as agents', 'jobs.agent_id', '=', 'agents.id', 'left')
                ->join('users as drivers', 'jobs.driver_id', '=', 'drivers.id', 'left');
        return $this;
    }

    public function buildReportQuery() {

        $this->query = Job::query()
                ->select('jobs.id as id as job_id', 'jobs.customer_name', 'jobs.customer_contact', 
                        'services.title as service', 'pickup_location.title as pickup_location', 'pickup_address', 'dropoff_location.title as dropoff_location', 'dropoff_address',
                        'jobs.pickup_date', 'jobs.pickup_time', 'jobs.comments',
                        'agents.name as agent', 'drivers.name as driver',
                        'jobs.no_of_person', 'jobs.rate as rate', 'total_charges', 'total_commission',
                        'jobs.status', 'jobs.payment_status'
                        
                )
                ->join('services', 'jobs.service_id', '=', 'services.id', 'left')
                ->join('locations as pickup_location', 'jobs.pickup_location_id', '=', 'pickup_location.id', 'left')
                ->join('locations as dropoff_location', 'jobs.dropoff_location_id', '=', 'dropoff_location.id', 'left')
                ->join('users as agents', 'jobs.agent_id', '=', 'agents.id', 'left')
                ->join('users as drivers', 'jobs.driver_id', '=', 'drivers.id', 'left');
        return $this;
    }
    
    public function buildTotalsQuery() {

        $this->query = Job::query()
                ->select(DB::raw('SUM(total_charges) as total'),
                        'agents.name as agent',
                         'jobs.status', 'jobs.payment_status as status'
                        
                )
                ->join('users as agents', 'jobs.agent_id', '=', 'agents.id', 'left')
                ->groupBy('agent')
                ->groupBy('payment_status')
                ->orderBy("agent", "asc");
        return $this;
    }
    
    public function buildCommissionQuery() {

        $this->query = Job::query()
                ->select(DB::raw('SUM(total_commission) as total'),
                        'agents.name as agent',
                         'jobs.status'
                        
                )
                ->join('users as agents', 'jobs.agent_id', '=', 'agents.id', 'left')
                ->groupBy('agent')
                ->orderBy("agent", "asc");
        return $this;
    }

    public function find($id) {
        return Job::findOrFail($id);
    }

    public function all() {
        return $this->applyFiltersToQuery($this->query())->get();
    }

    public function paginate($pagesize) {
        return $this->applyFiltersToQuery($this->query())->paginate($pagesize);
    }

    public function findBySlug($slug) {
        return $this->query()->where('slug', $slug)->first();
    }

    public function filterByAgent($value) {
        $this->query()->where('jobs.agent_id', '=', "$value");
    }

    public function filterByDriver($value) {
        $this->query()->where('jobs.driver_id', '=', "$value");
    }
    
    public function filterByCampBoss($value) {
        $this->query()->where('jobs.camp_boss_id', '=', "$value");
    }

    public function filterByPickupLocation($value) {
        $this->query()->where('jobs.pickup_location_id', '=', "$value");
    }

    public function filterByDropoffLocation($value) {
        $this->query()->where('jobs.dropoff_location_id', '=', "$value");
    }

    public function filterByCustomerName($value) {
        $this->query()->where('jobs.customer_name', 'like', "%$value%");
    }
    
    public function filterByDriverName($value) {
        $this->query()->where(function($query) use ($value)
            {
                $query->where('drivers.name', 'like', "%$value%")
                ->orWhere('jobs.extra_drivers', 'like', "%$value%");
            });
    }
    
    public function filterByPickupDate($value) {
        $this->query()->where('jobs.pickup_date', '=', $value);
    }
    
    public function filterByFrom($value) {
        $this->query()->where('jobs.pickup_date', '>=', $value);
    }
    
    public function filterByTo($value) {
        $this->query()->where('jobs.pickup_date', '<=', $value);
    }
    
    public function filterByCurrent($value) {
        $this->query()->where('jobs.pickup_date', '<=', new \DateTime('today'));
    }
    
    public function filterByToday($value) {
        $this->query()->where('jobs.pickup_date', '=', new \DateTime('today'));
    }

}
