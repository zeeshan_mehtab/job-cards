<?php

namespace App\Repositories\Contracts;

/*
 * Repository Interface for Categories Repository
 */

interface PageRepositoryInterface extends RepositoryInterface {

    public function findBySlug($slug);
    
    public function getList($group);
    
}
