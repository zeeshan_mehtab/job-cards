<?php

namespace App\Repositories\Contracts;

/*
 * Repository Interface for Categories Repository
 */

interface VehicleRepositoryInterface extends RepositoryInterface {

    public function findByCategory($cat);

}
