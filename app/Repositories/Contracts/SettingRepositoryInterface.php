<?php

namespace App\Repositories\Contracts;

/*
 * Repository Interface for Categories Repository
 */

interface SettingRepositoryInterface extends RepositoryInterface {

    public function findByKey($key);
}
