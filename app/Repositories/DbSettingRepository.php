<?php

namespace App\Repositories;

/*
 * Repository to fetch ingredients from DB
 */

use App\Repositories\Contracts\SettingRepositoryInterface;
use App\Setting;
use App\Repositories\DbRepository;

class DbSettingRepository extends DbRepository implements SettingRepositoryInterface {

    public function __construct() {
        $this->validFilterableFields = ['key', 'value'];
        $this->query = Setting::query();
    }

    public function find($id) {
        return Setting::findOrFail($id);
    }

    public function all() {
        return $this->applyFiltersToQuery($this->query())->get();
    }

    public function paginate($pagesize) {
        return $this->applyFiltersToQuery($this->query())->paginate($pagesize);
    }

    public function findByKey($key) {
        $row = $this->query()->where('key', $key)->first();
        if ($row) {
            return $row->value;
        } else {
            return null;
        }
    }

    public function refresh() {
        $this->query = Setting::query();
    }

}
