<?php

namespace App\Repositories;

/*
 * Repository to fetch ingredients from DB
 */

use App\Repositories\Contracts\VehicleRepositoryInterface;
use App\Models\Vehicle;
use App\Repositories\DbRepository;

class DbVehicleRepository extends DbRepository implements VehicleRepositoryInterface {

    public function __construct() {
        $this->validFilterableFields = ['category', 'manufacturer', 'year', 'identification_no', 'identification_no_like', 'description_like'];
        $this->query = Vehicle::query()
                ->select('vehicles.*', 'categories.name as category', 'categories.slug as category_slug', 'manufacturers.name as manufacturer')
                ->join('categories', 'vehicles.category_id', '=', 'categories.id')
                ->join('manufacturers', 'vehicles.manufacturer_id', '=', 'manufacturers.id')
                ->where('vehicles.status', '=', 'available');
    }

    public function find($id) {
        return Vehicle::findOrFail($id);
    }

    public function all() {
        return $this->applyFiltersToQuery($this->query())->get();
    }

    public function paginate($pagesize) {
        return $this->applyFiltersToQuery($this->query())->paginate($pagesize);
    }

    public function findBySlug($slug) {
        return $this->query()->where('slug', $slug)->first();
    }

    public function findByCategory($cat) {
        return $this->query->where('categories.slug', $cat)->get();
    }

    public function filterByCategory($value) {
        $this->query()->where('vehicles.category_id', '=', "$value");
    }

    public function filterByManufacturer($value) {
        $this->query()->where('vehicles.manufacturer_id', '=', "$value");
    }

    public function filterByYear($value) {
        $this->query()->where('vehicles.model_year', '=', "$value");
    }

    public function filterByIdentificationNoLike($value) {
        $this->query()->where('vehicles.identification_no', 'like', "%$value%");
    }

    public function filterByDescriptionLike($value) {
        $this->query()->where('vehicles.description_en', 'like', "%$value%");
        $this->query()->orWhere(function ($query) use($value) {
            $query->where('vehicles.description_ar', 'like', "%$value%");
        });
    }

}
