<?php

namespace App\Listeners;

use App\Events\JobWasCreated;

class JobWasCreatedSetFields {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasVerified  $event
     * @return void
     */
    public function handle(JobWasCreated $event) {

        $job = $event->job;
        $total_charges = $job->no_of_person_charged * $job->service->rate;
        $job->rate = $job->service->rate;
        $job->total_charges = $total_charges;
        $job->commission = $job->agent->commission;
        $job->total_commission = ($total_charges * $job->agent->commission) / 100;
        $job->save();
    }

}
