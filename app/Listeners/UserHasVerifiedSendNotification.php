<?php

namespace App\Listeners;

use App\Events\UserHasVerified;
use Illuminate\Support\Facades\Mail;

class UserHasVerifiedSendNotification {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasVerified  $event
     * @return void
     */
    public function handle(UserHasVerified $event) {
        $user = $event->user;
        Mail::send('emails.welcome', ['user' => $user], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Thank you for joining News Stand');
        });
    }

}
