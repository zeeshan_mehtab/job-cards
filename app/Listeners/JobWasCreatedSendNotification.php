<?php

namespace App\Listeners;

use App\Events\JobWasCreated;
use Illuminate\Support\Facades\Mail;

class JobWasCreatedSendNotification {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasVerified  $event
     * @return void
     */
    public function handle(JobWasCreated $event) {

        $job = $event->job;
        Mail::send('emails.new_job', ['job' => $job], function ($m) use ($job) {
            $m->to("guna@afriditravel.com", "ATNT Job Card Manager")->subject('New Job Card Created');
            //$m->to("zeeshan.mehtab1@gmail.com", "ATNT Job Card Manager")->subject('New Job Card Created');
        });
    }

}
