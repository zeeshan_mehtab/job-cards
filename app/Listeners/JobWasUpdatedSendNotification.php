<?php

namespace App\Listeners;

use App\Events\JobWasUpdated;
use Illuminate\Support\Facades\Mail;

class JobWasUpdatedSendNotification {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasVerified  $event
     * @return void
     */
    public function handle(JobWasUpdated $event) {

        $job = $event->job;
        $oldJob = $event->oldJob;
        
        if($oldJob->status != 'approved' && $job->status == 'approved') {
            Mail::send('emails.approved_job', ['job' => $job], function ($m) use ($job) {
                $m->to($job->agent->email, $job->agent->name)->subject('Job Was Approved');
            });
        } else if($oldJob->status != 'cancelled' && $job->status == 'cancelled') {
            Mail::send('emails.canceled_job', ['job' => $job], function ($m) use ($job) {
                $m->to($job->agent->email, $job->agent->name)->subject('Job Was Canceled');
            });
        }
    }

}
