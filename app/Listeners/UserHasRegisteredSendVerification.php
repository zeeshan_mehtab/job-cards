<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Support\Facades\Mail;
use App\Events\UserHasRegistered;

class UserHasRegisteredSendVerification {

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserHasRegistered $event) {
        $user = $event->user;
        Mail::send('emails.verify', ['confirmation_code' => $user->confirmation_code], function ($m) use ($user) {
            $m->to($user->email, $user->name)->subject('Verify your email address');
        });
    }

}
