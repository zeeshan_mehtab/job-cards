<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model {

    use SoftDeletes;

    protected $fillable = [
        'identification_no',
        'description',
        'category_id',
        'manufacturer_id',
        'model_year',
        'exterior_colour',
        'status',
    ];
    protected $dates = ['deleted_at'];

    public function category() {
        return $this->belongsTo("App\Models\Category");
    }

    public function manufacturer() {
        return $this->belongsTo("App\Models\Manufacturer");
    }

}
