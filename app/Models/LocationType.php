<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LocationType extends Model {

    use SoftDeletes;

    protected $fillable = [
        'title',
    ];
    protected $dates = ['deleted_at'];

    public function locations() {
        return $this->hasMany('App\Models\Location');
    }

}
