<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Location extends Model {

    use SoftDeletes;

    protected $fillable = [
        'title',
        'google_map',
        'address',
        'makani',
        'location_type_id',
    ];
    protected $dates = ['deleted_at'];
    
    public function location_type() {
        return $this->belongsTo('App\Models\LocationType');
    }

}
