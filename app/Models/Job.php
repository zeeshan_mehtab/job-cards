<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model {

    use SoftDeletes;

    protected $fillable = [
        'customer_name',
        'customer_contact',
        'service_id',
        'no_of_person',
        'no_of_person_charged',
        'pickup_location_id',
        'dropoff_location_id',
        'pickup_address',
        'dropoff_address',
        'comments',
        'rate',
        'commission',
        'total_charges',
        'total_commission',
        'model_year',
        'pickup_date',
        'start_date',
        'end_date',
        'pickup_time',
        'agent_id',
        'driver_id',
        'extra_drivers',
        'vehicle_plate_no',
        'status',
        'payment_status',
        'camp_boss_id',
    ];
    protected $dates = ['deleted_at', 'pickup_date', 'start_date', 'end_date'];

    public function agent() {
        return $this->belongsTo("App\Models\User", "agent_id", "id");
    }

    public function driver() {
        return $this->belongsTo("App\Models\User", "driver_id", "id");
    }
    
    public function location() {
        return $this->belongsTo("App\Models\Location");
    }
    
    public function service() {
        return $this->belongsTo("App\Models\Service");
    }
    
    public function logs() {
        return $this->hasMany("App\Models\Log");
    }
    
    public function dropoff_location() {
        return $this->belongsTo("App\Models\Location", "dropoff_location_id", "id");
    }
    
    public function pickup_location() {
        return $this->belongsTo("App\Models\Location", "pickup_location_id", "id");
    }

}
