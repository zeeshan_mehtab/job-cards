<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model {

    use SoftDeletes;

    protected $fillable = [
        'comments',
        'author_id',
        'job_id',
    ];
    protected $dates = ['deleted_at'];
    
    public function job() {
        return $this->belongsTo('App\Models\Job');
    }
    
    public function author() {
        return $this->belongsTo("App\Models\User", "author_id", "id");
    }

}
