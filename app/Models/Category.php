<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model {
    
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'slug',
        'main_category'
    ];
    
    protected $dates = ['deleted_at'];
    
    public function vehicles() {
        return $this->hasMany("App\Vehicle");
    }

}
