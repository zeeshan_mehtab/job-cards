<?php

namespace App\Models;

/*
 * This file is part of News Stand Project.
 * Copyright (c) 2015 Zeeshan Mehtab.
 * This file is open source and can be used and redistributed
 */

/**
 * Description of Permission
 *
 * @author Zeeshan
 */
use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission {
    
}
