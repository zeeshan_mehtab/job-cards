@extends('front.layouts.default')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Update Profile</div>
                <div class="panel-body">
                    {!! Form::model($user, ['action' =>  ['Front\ProfilesController@update', $user->id], 'method' => 'put']) !!}
                        
                        <div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
                            {!! Form::label('name', 'Display Name:') !!}
                            {!! Form::text('name', $user->name, array('class' => 'form-control', 'placeholder' => 'Display Name')) !!}
                        </div>
                    
                        <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::text('email', $user->email, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                Update
                            </button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

{{-- Page Heading --}}
@section('page-heading')
Update Profile
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Update Profile</a>
</li>
@stop