@extends('front.layouts.home')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="jumbotron text-center">
            <h1>Afridi Travel & Tourism Web Portal</h1>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12 text-center">
        @if (Auth::guest())
            <div class="col-lg-4 col-lg-offset-4">
                <i class="fa fa-sign-in fa-5x"></i><br/>
                <h3><a href="{{ url('/') }}/auth/login">Login</a></h3>
            </div>
        @else
            @if (Entrust::hasRole('account'))
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <i class="fa fa-bar-chart fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin">View Dashboard</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-th-list fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/account/jobs">View Job Cards</a></h3>
                </div>
                <div class="col-lg-3"></div>
            @endif
            
            @if (Entrust::hasRole('camp_boss'))
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <i class="fa fa-bar-chart fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin">View Dashboard</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-th-list fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/camp/jobs">View Job Cards</a></h3>
                </div>
                <div class="col-lg-3"></div>
            @endif
            
            @if (Entrust::hasRole('agent'))
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <i class="fa fa-bar-chart fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin">View Dashboard</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-th-list fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/agent/jobs">Manage Job Cards</a></h3>
                </div>
                <div class="col-lg-3"></div>
            @endif

            @if (Entrust::hasRole('driver'))
                <div class="col-lg-3"></div>
                <div class="col-lg-3">
                    <i class="fa fa-bar-chart fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin">View Dashboard</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-th-list fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/driver/jobs">Manage Job Cards</a></h3>
                </div>
                <div class="col-lg-3"></div>
            @endif

            @if (Entrust::hasRole('super-admin') || Entrust::hasRole('admin'))
                <div class="col-lg-3">
                    <i class="fa fa-bar-chart fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin">View Dashboard</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-th-list fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/jobs">Manage Job Cards</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-truck fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/vehicles">Manage Vehicles</a></h3>
                </div>
                <div class="col-lg-3">
                    <i class="fa fa-map-marker fa-5x"></i><br/>
                    <h3><a href="{{ url('/') }}/admin/listing/locations">Manage Locations</a></h3>
                </div>
            @endif
            
        @endif
    </div>
</div>
<div class="row">
    <div class="col-lg-12 text-center" style="height: 100px;">
        
    </div>
</div>
@endsection
