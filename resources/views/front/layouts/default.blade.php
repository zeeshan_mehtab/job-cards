<!DOCTYPE html>
<html lang="en">
    @include('front.partials.head')
    <body>
        @include('front.partials.header')
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">@yield('page-heading')
                        <small>@yield('page-subheading')</small>
                    </h1>
                </div>
            </div>
            @include('front.partials.breadcrumb')
            @include('front.partials.notifications')
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
            <hr>
            @include('front.partials.footer')
        </div>
    </body>
</html>
