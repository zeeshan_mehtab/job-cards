<!DOCTYPE html>
<html lang="en">
    @include('front.partials.head')
    <body>
        @include('front.partials.header')
        <div class="container">
            @include('front.partials.notifications')
            <div class="row">
                <div class="col-md-12">
                    @yield('content')
                </div>
            </div>
            <hr>
            @include('front.partials.footer')
        </div>
    </body>
</html>
