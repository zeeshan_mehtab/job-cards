<div class="row">
    <div class="col-lg-12">
        @if ($message = Session::get('success'))
        <div class="alert alert-success" role="alert">
            <strong>Success!</strong> {{ $message }}
        </div>
        {{ Session::forget('success') }}
        @endif

        @if ($message = Session::get('error'))
        <div class="alert alert-danger" role="alert">
            <strong>Whoops!</strong> {{ $message }}
        </div>
        {{ Session::forget('error') }}
        @endif

        @if (count($errors) > 0)
        <div class="alert alert-danger" role="alert">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>