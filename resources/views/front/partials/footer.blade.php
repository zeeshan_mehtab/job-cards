<footer>
    <div class="row">
        <div class="col-lg-6">
            <p>© Copyright 2015, ATNT Job Cards</p>
        </div>
        <div class="col-lg-6 text-right"></div>
    </div>
</footer>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="/js/app.js"></script>

<!-- BEGIN PAGE LEVEL STYLES -->
@section('pagelevel-scripts')

@show
<!-- END PAGE LEVEL STYLES -->