@if(count($news) > 0)
<table class="table table-striped table-hover table-bordered">
    <thead>
        <tr>
            <th>Title</th>
            <th>Author</th>
            <th>Date</th>
            @if (Auth::user())
            <th class="text-right">Options</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($news as $n)
        <tr>
            <td><a href="{{ action('NewsController@show', array($n->id)) }}">{{ $n->title }}</a></td>
            <td>{{ $n->user->name }}</td>
            <td>{{ $n->created_at }}</td>
            @if (Auth::user())
            <td class="text-right">
                @can('own-news', $n)
                {!! Form::open(['action' =>  ['NewsController@destroy', $n->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                {!! Form::close() !!}
                @endcan
            </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>
@else
<div class="note note-warning">No News Found</div>
@endif