@extends('front.layouts.default')

@section('page-heading')
{{ $news->title }}
@endsection

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-lg-6 news-photos">
        <img class="img-responsive center-block" src="{{ URL::to('/') }}/{{ $news->image }}" alt="{{ $news->title }}">
        <hr/>
    </div>
    <div class="col-md-6 news-detail">
        <div class="row">
            <h4>{{ $news->title }}</h4>
            {!! $news->detail !!}
        </div>
        <div class="row">
            <div class="col-md-12 text-right">
                <div class="btn-group">
                    <button class="btn btn-primary" onClick="location.href ='{{ action('NewsController@pdf', array($news->id)) }}'">
                        Download As PDF <i class="fa fa-download"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>    

@stop


{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('NewsController@index') }}">News</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">{{ $news->title }}</a>
</li>
@stop