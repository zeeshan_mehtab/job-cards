@extends('front.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        {!! Form::open(array('action' => 'NewsController@store', 'enctype' => 'multipart/form-data')) !!}

        @include('front.news.form', ['submitButtonText' => 'Add News'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create News
@stop

{{-- Page Heading --}}
@section('page-heading')
Create News
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('NewsController@index') }}">News</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create News</a>
</li>
@stop