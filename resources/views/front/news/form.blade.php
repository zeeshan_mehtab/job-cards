<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) !!}
</div>

<div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
    {!! Form::label('detail', 'Detail:') !!}
    {!! Form::textarea('detail', null, array('class' => 'form-control', 'placeholder' => 'Detail')) !!}
</div>

<div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, array('class' => 'form-control', 'placeholder' => 'Image')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>

@section('pagelevel-scripts')
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>
@stop