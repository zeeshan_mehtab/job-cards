@extends('front.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        @include('front.news.list')
    </div>
</div>

@if (Auth::user())
<div class="row">
    <div class="col-md-12 text-right">
        <div class="btn-group">
            <button class="btn btn-primary" onClick="location.href ='{{ URL::action('NewsController@create') }}'">
                Add News <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
</div>
@endif

@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('page-heading')
Latest News
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Latest News</a>
</li>
@stop