
@extends('emails.layouts.basic')

@section('content')
<h2>Reset Your Email Address</h2>

<div>
    <p>Click here to reset your password: {{ url('password/reset/'.$token) }}</p>
</div>
@endsection