@extends('emails.layouts.basic')

@section('content')
<h2>Verify Your Email Address</h2>

<div>
    <p>
    Thanks for creating an account with the ATNT Job Cards.</p>
    <p>Please follow the link below to verify your email address.</p>
    <p>{{ URL::to('register/verify/' . $confirmation_code) }}</p>
</div>
@endsection