@extends('emails.layouts.basic')

@section('content')
<h2>Thank you for joining ATNT Job Cards</h2>

<div>
    <p>
    Thanks for creating an account with the ATNT Job Cards.</p>
    <p>You can now log into your account and publish news.</p>
</div>
@endsection