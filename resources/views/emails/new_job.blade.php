@extends('emails.layouts.basic')

@section('content')
<div>
    <p>Dear Job Cards Manager,<br/><br/>
        A new job has been created by {{ $job->agent->name }} with following details<br/><br/>
        Service: {{ $job->service->title }}<br/>
        Pax: {{ $job->no_of_person }}<br/>
        Chargeable Pax: {{ $job->no_of_person_charged }}<br/>
        Pickup Date: {{ $job->pickup_date }}<br/>
        Pickup Time: {{ $job->pickup_time }}<br/><br/>
        Please click <a href="{{ url('/') }}/admin/listing/jobs">here</a> to view more details 
    </p>
</div>
@endsection