@extends('emails.layouts.basic')

@section('content')
<div>
    <p>Dear { $job->agent->name }},<br/>
        A job has been approved by job cards manager with following details<br/><br/>
        Service: {{ $job->service->title }}<br/>
        Pax: {{ $job->no_of_person }}<br/>
        Chargeable Pax: {{ $job->no_of_person_charged }}<br/>
        Pickup Date: {{ $job->pickup_date }}<br/>
        Pickup Time: {{ $job->pickup_time }}<br/><br/>
        Driver Name: {{ $job->driver->name }}<br/><br/>
        Please click <a href="{{ url('/') }}/admin/listing/agent/jobs">here</a> to view more details. 
    </p>
</div>
@endsection