@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <h2>Admin Dashboard</h2>
        <div class="row">
            @include('admin.dashboard.charts.jobs-per-day-html')
            @include('admin.dashboard.charts.jobs-per-service-html')
        </div>
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Admin <small>Dashboard</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')

@stop

@section('pagelevel-scripts')

<style>
    .chart {
        width		: 100%;
        height		: 500px;
        font-size	: 11px;
    }	
</style>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/none.js"></script>

@include('admin.dashboard.charts.jobs-per-day-script')
@include('admin.dashboard.charts.jobs-per-service-script')


@stop