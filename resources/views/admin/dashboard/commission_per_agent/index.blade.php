@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

@include('admin.dashboard.commission_per_agent.filters')

<div class="row">
    <div class="col-md-12">
        @if(count($jobs) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Officer</th>
                    <th>Total Commission</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobs as $row)
                <tr>
                    <td>{{ $row->agent }}</td>
                    <td>{{ $row->total }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <div class="note note-warning">No date for selected dates</div>
        @endif
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Totals Per Officer
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Totals Per Officer</a>
</li>
@stop