{!! Form::model($input, ['url' =>  '/admin/reports/commission-per-agent', 'method' => 'get', 'id' => 'filter-form']) !!}
<div class="row">
    <div class="table-toolbar">
        <div class="col-md-4">
            {!! Form::text('from', null, array('id' => 'from' ,'class' => 'form-control', 'placeholder' => 'From Date')) !!}
        </div>
        <div class="col-md-4">
            {!! Form::text('to', null, array('id' => 'to' ,'class' => 'form-control', 'placeholder' => 'To Date')) !!}
        </div>
        <div class="col-md-4">
            {!! Form::submit('Filter Data', array('class' => 'btn btn-primary')) !!}
        </div>
    </div>
</div>

@section('pagelevel-scripts')
<script src="/js/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker.css" rel="stylesheet" type="text/css"/>
<script>

    var today = new Date();
    
    $('#from').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true,
        defaultDate: today,
    });
    
    $('#to').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true,
        defaultDate: today,
    });
</script>
@stop


{!! Form::close() !!}