<div class="col-md-6 col-sm-6">
    <!-- BEGIN PORTLET-->
    <div class="portlet solid bordered grey-cararra">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-bar-chart-o"></i>Jobs Per Service- Last 30 Days
            </div>
        </div>
        <div class="portlet-body">
            <div id="site_statistics_loading" style="display: none;">
                <img src="/img/loading.gif" alt="loading">
            </div>
            <div id="site_statistics_content" class="display-none" style="display: block;">
                <div id="jobs_per_service_chart" class="chart"></div>
            </div>
        </div>
    </div>
    <!-- END PORTLET-->
</div>