<script>

    $.getJSON("/admin/reports/jobs-per-service", function (data) {
        var chart = AmCharts.makeChart("jobs_per_service_chart", {
            "type": "serial",
            "theme": "none",
            "dataProvider": data,
            "valueAxes": [{
                    "gridColor": "#FFFFFF",
                    "gridAlpha": 0,
                    "dashLength": 0,
                    "labelFrequency": 0,
                }],
            "gridAboveGraphs": true,
            "startDuration": 1,
            "graphs": [{
                    "balloonText": "[[service]]: <b>[[total]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "total"
                }],
            "chartCursor": {
                "categoryBalloonEnabled": false,
                "cursorAlpha": 0,
                "zoomable": false
            },
            "categoryField": "service",
            "categoryAxis": {
                "gridPosition": "start",
                "gridAlpha": 0,
                "tickPosition": "start",
                "tickLength": 10
            },
            "exportConfig": {
                "menuTop": 0,
                "menuItems": [{
                        "icon": '/lib/3/images/export.png',
                        "format": 'png'
                    }]
            }
        });
        chart.write("jobs_per_service_chart");
    });

</script>
