<script>

    $.getJSON("/admin/reports/jobs-per-day", function (data) {
        console.debug(data);
// SERIAL CHART    
        chart = new AmCharts.AmSerialChart();
        chart.pathToImages = "http://www.amcharts.com/lib/images/";
        chart.dataProvider = data;
        chart.categoryField = "date";
        chart.dataDateFormat = "YYYY-MM-DD";

// GRAPHS

        var graph1 = new AmCharts.AmGraph();
        graph1.valueField = "total";
        graph1.bullet = "round";
        graph1.bulletBorderColor = "#FFFFFF";
        graph1.bulletBorderThickness = 2;
        graph1.lineThickness = 2;
        graph1.lineAlpha = 0.5;
        chart.addGraph(graph1);

        var graph2 = new AmCharts.AmGraph();
        graph2.valueField = "total";
        graph2.bullet = "round";
        graph2.bulletBorderColor = "#FFFFFF";
        graph2.bulletBorderThickness = 2;
        graph2.lineThickness = 2;
        graph2.lineAlpha = 0.5;
        chart.addGraph(graph2);

// CATEGORY AXIS 
        chart.categoryAxis.parseDates = true;

// WRITE
        chart.write("jobs_per_day_chart");
    });

</script>
