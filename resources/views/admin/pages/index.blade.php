@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('PagesController@create') }}'">
                            Add New Page <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($pages) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Page</th>
                    <th>Slug</th>
                    <th>Group</th>
                    <th>Content</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pages as $page)
                <tr>
                    <td><a href="{{ action('PagesController@show', array($page->id)) }}">{{ $page->title }}</a></td>
                    <td>{{ $page->slug }}</td>
                    <td>{{ $page->group }}</td>
                     <td>{!! substr($page->content, 0, 500) !!} ...</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('PagesController@edit', array($page->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['PagesController@destroy', $page->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $pages->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Pages Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Pages <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Pages</a>
</li>
@stop