@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        {!! Form::model($page, ['action' =>  ['PagesController@update', $page->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

        <h2>Edit Page <small>{{ $page->name }}</small></h2>

        @include('admin.pages.form', ['submitButtonText' => 'Update Page'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Page {{ $page->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Pages <small>Edit Page: {{ $page->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('PagesController@index') }}">Pages</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Page {{ $page->name }}</a>
</li>
@stop