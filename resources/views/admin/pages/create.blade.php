@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        {!! Form::open(array('action' => 'PagesController@store', 'enctype' => 'multipart/form-data')) !!}
        <h2>Create New Page</h2>

        @include('admin.pages.form', ['submitButtonText' => 'Add Page'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Page
@stop

{{-- Page Heading --}}
@section('heading')
Pages <small>Create New Page</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('PagesController@index') }}">Pages</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Page</a>
</li>
@stop