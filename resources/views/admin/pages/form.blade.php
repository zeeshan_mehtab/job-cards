<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Name')) !!}
</div>

<div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
    {!! Form::label('title', 'Page Title:') !!}
    {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Page Title')) !!}
</div>

<div class="form-group {{ ($errors->has('slug')) ? 'has-error' : '' }}">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, array('class' => 'form-control', 'placeholder' => 'Slug')) !!}
</div>


<div class="form-group {{ ($errors->has('content')) ? 'has-error' : '' }}">
    {!! Form::label('content', 'Description:') !!}
    {!! Form::textarea('content', null, array('class' => 'form-control', 'placeholder' => 'Content')) !!}
</div>

<div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::file('image', null, array('class' => 'form-control', 'placeholder' => 'Image')) !!}
</div>

<div class="form-group {{ ($errors->has('group')) ? 'has-error' : '' }}">
    {!! Form::label('group', 'Page Group:') !!}
    {!! Form::text('group', null, array('class' => 'form-control', 'placeholder' => 'Page Group')) !!}
</div>

<div class="form-group {{ ($errors->has('sort_order')) ? 'has-error' : '' }}">
    {!! Form::label('sort_order', 'Page Group:') !!}
    {!! Form::input('number','sort_order', null, array('class' => 'form-control', 'placeholder' => 'Sort Order')) !!}
</div>


<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>

@section('pagelevel-scripts')
<script src="//tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script>tinymce.init({selector: 'textarea'});</script>
@stop