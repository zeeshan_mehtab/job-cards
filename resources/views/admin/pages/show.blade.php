@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h2>Page Details</h2>
<table class="table table-striped table-hover table-bordered">
    <tbody>
        <tr>
            <td><strong>Title</strong>: {{ $page->title }}</td>
            <td><strong>Slug</strong>: {{ $page->slug }}</td>
            <td><strong>Group</strong>: {{ $page->group }}</td>
            <td><strong>Sort Order</strong>: {{ $page->sort_order }}</td>
            <td class="text-right">
                <button class="btn btn-primary" onClick="location.href ='{{ action('PagesController@edit', array($page->id)) }}'">Edit Page</button>
            </td>
        </tr>
        <tr>
            <td colspan="5">{!! $page->content !!}</td>
        </tr>    
    </tbody>
</table>

@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Page {{ $page->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Pages <small>Show Page: {{ $page->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('PagesController@index') }}">Pages</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">{{ $page->name }}</a>
</li>
@stop
