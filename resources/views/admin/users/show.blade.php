@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $user['name'] }} User</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('Auth\UsersController@edit', array($user->id)) }}'">Edit User</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show User {{ $user->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Users <small>Show User: {{ $user->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\UsersController@index') }}">Users</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show User {{ $user->name }}</a>
</li>
@stop
