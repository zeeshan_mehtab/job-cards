@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Auth\UsersController@create') }}'">
                            Add New User <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($users) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>User</th>
                    <th>Email</th>
                    <th>Roles</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $user)
                <tr>
                    <td><a href="{{ action('Auth\UsersController@show', array($user->id)) }}">{{ $user->name }}</a></td>
                    <td>{{ $user->email }}</td>
                    <td>
                        @foreach($user->roles as $role )
                        {{ $role->display_name }},<br/>
                        @endforeach
                    </td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Auth\UsersController@edit', array($user->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Auth\UsersController@destroy', $user->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $users->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Users Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Users <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Users</a>
</li>
@stop