<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Name')) !!}
</div>

<div class="form-group {{ ($errors->has('user')) ? 'has-error' : '' }}">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
</div>

<div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
    {!! Form::label('password', 'Password:') !!}
    <input class="form-control" placeholder="Password" name="password" type="password" value="" id="password">
</div>

<div class="form-group {{ ($errors->has('role_list')) ? 'has-error' : '' }}">
    {!! Form::label('role_list', 'Roles:') !!}
    {!! Form::select('role_list[]', $roles,  isset($selected_roles) ? $selected_roles : null, array('class' => 'form-control', 'multiple' => 'multiple')) !!}
</div>

<div class="form-group {{ ($errors->has('commission')) ? 'has-error' : '' }}">
    {!! Form::label('commission', 'Commission:') !!}
    {!! Form::number('commission', null, array('class' => 'form-control', 'placeholder' => 'Commission')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>