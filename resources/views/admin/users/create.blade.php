@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Auth\UsersController@store')) !!}
        <h2>Create New User</h2>

        @include('admin.users.form', ['submitButtonText' => 'Add User'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create User
@stop

{{-- Page Heading --}}
@section('heading')
Users <small>Create New User</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\UsersController@index') }}">Users</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New User</a>
</li>
@stop