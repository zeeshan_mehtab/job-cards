@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($user, ['action' =>  ['Auth\UsersController@update', $user->id], 'method' => 'patch']) !!}

        <h2>Edit User <small>{{ $user->name }}</small></h2>

        @include('admin.users.form', ['submitButtonText' => 'Update User'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit User {{ $user->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Users <small>Edit User: {{ $user->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\UsersController@index') }}">Users</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit User {{ $user->name }}</a>
</li>
@stop