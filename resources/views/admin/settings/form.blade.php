<div class="form-group {{ ($errors->has('key')) ? 'has-error' : '' }}">
    {!! Form::label('key', 'Key:') !!}
    {!! Form::text('key', null, array('class' => 'form-control', 'placeholder' => 'Key')) !!}
</div>

<div class="form-group {{ ($errors->has('value')) ? 'has-error' : '' }}">
    {!! Form::label('value', 'Value:') !!}
    {!! Form::text('value', null, array('class' => 'form-control', 'placeholder' => 'Value')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>