@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($setting, ['action' =>  ['Admin\SettingsController@update', $setting->id], 'method' => 'patch']) !!}

        <h2>Edit Setting <small>{{ $setting->key }}</small></h2>

        @include('admin.settings.form', ['submitButtonText' => 'Update Setting'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Setting {{ $setting->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Settings <small>Edit Setting: {{ $setting->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\SettingsController@index') }}">Settings</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Setting {{ $setting->name }}</a>
</li>
@stop