@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $setting['key'] }} Setting</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('SettingsController@edit', array($setting->id)) }}'">Edit Setting</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Setting {{ $setting->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Settings <small>Show Setting: {{ $setting->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('SettingsController@index') }}">Settings</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Setting {{ $setting->name }}</a>
</li>
@stop
