@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Admin\SettingsController@store')) !!}
        <h2>Create New Setting</h2>
        
        @include('admin.settings.form', ['submitButtonText' => 'Create Setting'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Setting
@stop

{{-- Page Heading --}}
@section('heading')
Settings <small>Create New Setting</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\SettingsController@index') }}">Settings</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Setting</a>
</li>
@stop