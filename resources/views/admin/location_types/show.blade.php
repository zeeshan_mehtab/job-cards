@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $location_type['title'] }} Location Type</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('LocationTypesController@edit', array($location_type->id)) }}'">Edit LocationType</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Location Type {{ $location_type->title }}
@stop

{{-- Page Heading --}}
@section('heading')
Location Types <small>Show Location Type: {{ $location_type->title }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('LocationTypesController@index') }}">Location Types</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Location Type {{ $location_type->name }}</a>
</li>
@stop
