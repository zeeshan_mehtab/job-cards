@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($location_type, ['action' =>  ['Admin\LocationTypesController@update', $location_type->id], 'method' => 'patch']) !!}

        <h2>Edit LocationType <small>{{ $location_type->key }}</small></h2>

        @include('admin.location_types.form', ['submitButtonText' => 'Update LocationType'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit LocationType {{ $location_type->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Location Types <small>Edit Location Type: {{ $location_type->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\LocationTypesController@index') }}">LocationTypes</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit LocationType {{ $location_type->name }}</a>
</li>
@stop