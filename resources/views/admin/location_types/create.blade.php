@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Admin\LocationTypesController@store')) !!}
        <h2>Create New LocationType</h2>
        
        @include('admin.location_types.form', ['submitButtonText' => 'Create Location Type'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Location Type
@stop

{{-- Page Heading --}}
@section('heading')
LocationTypes <small>Create New Location Type</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\LocationTypesController@index') }}">LocationTypes</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Location Type</a>
</li>
@stop