@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        {!! Form::open(array('action' => 'Admin\LocationsController@store')) !!}
        <h2>Create New Location</h2>
        
        @include('admin.locations.form', ['submitButtonText' => 'Create Location'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Location
@stop

{{-- Page Heading --}}
@section('heading')
Locations <small>Create New Location</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\LocationsController@index') }}">Locations</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Location</a>
</li>
@stop