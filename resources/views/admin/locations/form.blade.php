<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) !!}
        </div>

        <div class="form-group {{ ($errors->has('location_type_id')) ? 'has-error' : '' }}">
            {!! Form::label('location_type_id', 'Location Type:') !!}
            {!! Form::select('location_type_id', $location_types, null, array('class' => 'form-control')) !!}
        </div>

        <div class="form-group">
            {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group {{ ($errors->has('google_map')) ? 'has-error' : '' }}">
            {!! Form::label('google_map', 'Google Map URL:') !!}
            {!! Form::text('google_map', null, array('class' => 'form-control', 'placeholder' => 'Google Map URL')) !!}
        </div>

        <div class="form-group {{ ($errors->has('makani')) ? 'has-error' : '' }}">
            {!! Form::label('makani', 'Makani:') !!}
            {!! Form::text('makani', null, array('class' => 'form-control', 'placeholder' => 'Makani')) !!}
        </div>

        <div class="form-group {{ ($errors->has('address')) ? 'has-error' : '' }}">
            {!! Form::label('address', 'Address:') !!}
            {!! Form::textarea('address', null, array('class' => 'form-control', 'placeholder' => 'Address')) !!}
        </div>
    </div>
</div>