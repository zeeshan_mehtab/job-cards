@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-10 col-md-offset-1">
        {!! Form::model($location, ['action' =>  ['Admin\LocationsController@update', $location->id], 'method' => 'patch']) !!}

        <h2>Edit Location <small>{{ $location->key }}</small></h2>

        @include('admin.locations.form', ['submitButtonText' => 'Update Location'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Location {{ $location->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Locations <small>Edit Location: {{ $location->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\LocationsController@index') }}">Locations</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Location {{ $location->name }}</a>
</li>
@stop