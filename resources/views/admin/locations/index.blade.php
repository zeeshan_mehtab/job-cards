@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\LocationsController@create') }}'">
                            Add New Location <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($locations) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Type</th>
                    <th>Makani</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($locations as $location)
                <tr>
                    <td>{{ $location->title }}</td>
                    <td>
                    @if ($location->location_type)
                    {{ $location->location_type->title }}
                    @endif
                    </td>
                    <td>{{ $location->makani }}</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Admin\LocationsController@edit', array($location->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\LocationsController@destroy', $location->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $locations->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Locations Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Locations <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Locations</a>
</li>
@stop