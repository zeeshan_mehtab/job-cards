@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $location['title'] }} Location</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('LocationsController@edit', array($location->id)) }}'">Edit Location</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Location {{ $location->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Locations <small>Show Location: {{ $location->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('LocationsController@index') }}">Locations</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Location {{ $location->name }}</a>
</li>
@stop
