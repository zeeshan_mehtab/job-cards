@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $service['title'] }} Service</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('ServicesController@edit', array($service->id)) }}'">Edit Service</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Service {{ $service->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Services <small>Show Service: {{ $service->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('ServicesController@index') }}">Services</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Service {{ $service->name }}</a>
</li>
@stop
