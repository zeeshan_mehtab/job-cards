<div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Title')) !!}
</div>

<div class="form-group {{ ($errors->has('rate')) ? 'has-error' : '' }}">
    {!! Form::label('rate', 'Rate:') !!}
    {!! Form::number('rate', null, array('class' => 'form-control', 'placeholder' => 'Rate')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>
