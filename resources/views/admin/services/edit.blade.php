@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {!! Form::model($service, ['action' =>  ['Admin\ServicesController@update', $service->id], 'method' => 'patch']) !!}

        <h2>Edit Service <small>{{ $service->key }}</small></h2>

        @include('admin.services.form', ['submitButtonText' => 'Update Service'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Service {{ $service->key }}
@stop

{{-- Page Heading --}}
@section('heading')
Services <small>Edit Service: {{ $service->key }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\ServicesController@index') }}">Services</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Service {{ $service->name }}</a>
</li>
@stop