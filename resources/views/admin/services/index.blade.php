@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\ServicesController@create') }}'">
                            Add New Service <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($services) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Service</th>
                    <th>Rate</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($services as $service)
                <tr>
                    <td>{{ $service->title }}</td>
                    <td>{{ $service->rate }} AED</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Admin\ServicesController@edit', array($service->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\ServicesController@destroy', $service->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $services->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Services Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Services <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Services</a>
</li>
@stop