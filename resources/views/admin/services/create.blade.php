@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        {!! Form::open(array('action' => 'Admin\ServicesController@store')) !!}
        <h2>Create New Service</h2>
        
        @include('admin.services.form', ['submitButtonText' => 'Create Service'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Service
@stop

{{-- Page Heading --}}
@section('heading')
Services <small>Create New Service</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\ServicesController@index') }}">Services</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Service</a>
</li>
@stop