{!! Form::model($filters, ['action' =>  ['Admin\DriversController@index'], 'method' => 'get', 'id' => 'filter-form']) !!}
<div class="row">
    <div class="table-toolbar">
        <div class="row">
            <div class="col-lg-10">
                <div class="col-lg-4 col-md-4 col-sm-6 margin-bottom-10">
                    {!! Form::text('customer_name', null, array('class' => 'form-control', 'placeholder' => 'Customer Name')) !!}
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 margin-bottom-10">
                    {!! Form::text('customer_contact', null, array('class' => 'form-control', 'placeholder' => 'Customer Contact')) !!}
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 margin-bottom-10">
                    {!! Form::text('from', null, array('id' => 'from' ,'class' => 'form-control', 'placeholder' => 'From Date')) !!}
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 margin-bottom-10">
                    {!! Form::text('to', null, array('id' => 'to' ,'class' => 'form-control', 'placeholder' => 'To Date')) !!}
                </div>
            </div>
            <div class="col-lg-2">
                <div class="col-md-12 margin-bottom-10">
                    {!! Form::submit('Filter Data', array('class' => 'btn btn-primary')) !!}
                </div>
                <div class="col-md-12 margin-bottom-10">
                    <a class="btn btn-primary" id="export-btn">Export Data</a>
                </div>
            </div>
        </div>
    </div>
</div>

@section('pagelevel-scripts')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker.css" rel="stylesheet" type="text/css"/>

<script>
    $('#from').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true,
    });
    $('#to').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true,
    });
    
</script>
<script>
    jQuery("#export-btn").click(function () {
        var action = "{{ action('Admin\DriversController@export') }}"
        jQuery('form').get(0).setAttribute('action', action);
        jQuery('form').get(0).submit();
    });
</script>
@stop

{!! Form::close() !!}