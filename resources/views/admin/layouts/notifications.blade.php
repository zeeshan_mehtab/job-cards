@if ($message = Session::get('success'))
<div class="Metronic-alerts alert alert-success fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Success:</strong> {{ $message }}
</div>
{{ Session::forget('success') }}
@endif

@if ($message = Session::get('error'))
<div class="Metronic-alerts alert alert-danger fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Error:</strong> {{ $message }}
</div>
{{ Session::forget('error') }}
@endif

@if ($message = Session::get('warning'))
<div class="Metronic-alerts alert alert-warning fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Warning:</strong> {{ $message }}
</div>
{{ Session::forget('warning') }}
@endif

@if ($message = Session::get('info'))
<div class="Metronic-alerts alert alert-info fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>FYI:</strong> {{ $message }}
</div>
{{ Session::forget('info') }}
@endif

@if($errors->any())

<div class="Metronic-alerts alert alert-danger fade in">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <ul>
        @foreach($errors->all() as $error)
        <li><strong>Error:</strong> {{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif