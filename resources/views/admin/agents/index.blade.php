@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

@include('admin.agents.filters')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\AgentsController@create') }}'">
                            Add New Job <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($jobs) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Job #</th>
                    <th>Customer Name</th>
                    <th>Customer Contact</th>
                    <th>Service</th>
                    <th>Location</th>
                    <th>Pax</th>
                    <th>Status</th>
                    <th>Total Commission</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobs as $job)
                <tr>
                    <td><a href="{{ action('Admin\AgentsController@show', array($job->id)) }}">{{ $job->id }}</a></td>
                    <td><a href="{{ action('Admin\AgentsController@show', array($job->id)) }}">{{ $job->customer_name }}</a></td>
                    <td>{{ $job->customer_contact }}</td>
                    <td>{{ $job->service }}</td>
                    <td>
                        <p>
                            Pickup: {{ $job->pickup_location }} {{ $job->pickup_address }}<br>
                            Dropoff: {{ $job->dropoff_location }} {{ $job->dropoff_address }}<br>
                            Pickup Date: {{ $job->pickup_date }}<br>
                            Pickup Time: {{ $job->pickup_time }}<br>
                        </p>
                    </td>
                    <td>
                        <p>
                            Pax: {{ $job->no_of_person }}<br>
                            Pax Charged: {{ $job->no_of_person_charged }}<br>
                        </p>
                    </td>
                    <td>{{ ucwords($job->status) }}</td>
                    <td>AED {{ $job->total_commission }}</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Admin\AgentsController@edit', array($job->id)) }}'">Edit</button>                        
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $jobs->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Jobs Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Jobs</a>
</li>
@stop