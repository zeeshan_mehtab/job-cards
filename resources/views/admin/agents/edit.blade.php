@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        {!! Form::model($job, ['action' =>  ['Admin\AgentsController@update', $job->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

        <h2>Edit Job <small>{{ $job->customer_name }}</small></h2>

        @include('admin.agents.form', ['submitButtonText' => 'Update Job'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Job {{ $job->customer_name }}
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>Edit Job: {{ $job->identification_no }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\JobsController@index') }}">Jobs</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Job {{ $job->identification_no }}</a>
</li>
@stop