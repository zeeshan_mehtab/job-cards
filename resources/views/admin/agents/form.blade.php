<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ ($errors->has('customer_name')) ? 'has-error' : '' }}">
            {!! Form::label('customer_name', 'Customer Name:') !!}
            {!! Form::text('customer_name', null, array('class' => 'form-control', 'placeholder' => 'Customer Name')) !!}
        </div>

        <div class="form-group {{ ($errors->has('customer_contact')) ? 'has-error' : '' }}">
            {!! Form::label('customer_contact', 'Customer Contact:') !!}
            {!! Form::text('customer_contact', null, array('class' => 'form-control', 'placeholder' => 'Customer Contact')) !!}
        </div>

        <div class="form-group {{ ($errors->has('no_of_person')) ? 'has-error' : '' }}">
            {!! Form::label('no_of_person', 'No of Person:') !!}
            {!! Form::number('no_of_person', null, array('class' => 'form-control', 'placeholder' => 'No of Person')) !!}
        </div>

        <div class="form-group {{ ($errors->has('no_of_person_charged')) ? 'has-error' : '' }}">
            {!! Form::label('no_of_person_charged', 'No of Person Charged:') !!}
            {!! Form::number('no_of_person_charged', null, array('class' => 'form-control', 'placeholder' => 'No of Person Charged')) !!}
        </div>

        <div class="form-group {{ ($errors->has('service_id')) ? 'has-error' : '' }}">
            {!! Form::label('service_id', 'Service:') !!}
            {!! Form::select('service_id', array('' => 'Select Service') + $all_services, null, array('class' => 'form-control')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('pickup_date')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_date', 'Pickup Date:') !!}
            {!! Form::text('pickup_date', null, array('id' => 'pickup_date' ,'class' => 'form-control', 'placeholder' => 'Pickup Date')) !!}
        </div>

        <div class="form-group {{ ($errors->has('pickup_time')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_time', 'Pickup Time:') !!}
            {!! Form::text('pickup_time', null, array('class' => 'form-control', 'placeholder' => 'Pickup Time')) !!}
        </div>

    </div>
    <div class="col-lg-6">
        <div class="form-group {{ ($errors->has('pickup_location_id')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_location_id', 'Pickup Location:') !!}
            {!! Form::select('pickup_location_id', array('' => 'Select Location') + $all_locations, null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group {{ ($errors->has('pickup_address')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_address', 'Specify Address:') !!}
            {!! Form::text('pickup_address', null, array('class' => 'form-control', 'placeholder' => 'Pickup Address')) !!}
        </div>

        <div class="form-group {{ ($errors->has('dropoff_location_id')) ? 'has-error' : '' }}">
            {!! Form::label('dropoff_location_id', 'Drop Off Location:') !!}
            {!! Form::select('dropoff_location_id', array('' => 'Select Location') + $all_locations, null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group {{ ($errors->has('dropoff_address')) ? 'has-error' : '' }}">
            {!! Form::label('dropoff_address', 'Specify Address:') !!}
            {!! Form::text('dropoff_address', null, array('class' => 'form-control', 'placeholder' => 'Dropoff Address')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('comments')) ? 'has-error' : '' }}">
            {!! Form::label('comments', 'Comments:') !!}
            {!! Form::text('comments', null, array('class' => 'form-control', 'placeholder' => 'Comments')) !!}
        </div>
    </div>
    
</div> 

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>

@section('pagelevel-scripts')

<script src="/js/jquery-ui.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker.css" rel="stylesheet" type="text/css"/>

<script>
$('#pickup_date').datepicker({
    dateFormat: 'yy-mm-dd',
    inline: true,
});
</script>

@stop