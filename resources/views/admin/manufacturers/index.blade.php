@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\ManufacturersController@create') }}'">
                            Add New Manufacturer <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($manufacturers) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($manufacturers as $manufacturer)
                <tr>
                    <td><a href="{{ action('Admin\ManufacturersController@show', array($manufacturer->id)) }}">{{ $manufacturer->name }}</a></td>
                    <td class="text-right">
                        <button class="btn btn-default" onClick="location.href ='{{ action('Admin\ManufacturersController@edit', array($manufacturer->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\ManufacturersController@destroy', $manufacturer->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $manufacturers->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Manufacturers Found</div>
        @endif
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Manufacturers <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Manufacturers</a>
</li>
@stop