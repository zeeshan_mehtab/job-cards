@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Admin\ManufacturersController@store')) !!}
        <h2>Create New Manufacturer</h2>

        @include('admin.manufacturers.form', ['submitButtonText' => 'Add Manufacturer'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Manufacturer
@stop

{{-- Page Heading --}}
@section('heading')
Manufacturers <small>Create New Manufacturer</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\ManufacturersController@index') }}">Manufacturers</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Manufacturer</a>
</li>
@stop