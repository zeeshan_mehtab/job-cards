@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($manufacturer, ['action' =>  ['Admin\ManufacturersController@update', $manufacturer->id], 'method' => 'patch']) !!}

        <h2>Edit Manufacturer <small>{{ $manufacturer->name }}</small></h2>

        @include('admin.manufacturers.form', ['submitButtonText' => 'Update Manufacturer'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Manufacturer {{ $manufacturer->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Manufacturers <small>Edit Manufacturer: {{ $manufacturer->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\ManufacturersController@index') }}">Manufacturers</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Manufacturer {{ $manufacturer->name }}</a>
</li>
@stop