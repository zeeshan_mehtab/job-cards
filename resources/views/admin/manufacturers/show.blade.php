@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $manufacturer['name'] }} Manufacturer</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('Admin\ManufacturersController@edit', array($manufacturer->id)) }}'">Edit Manufacturer</button>
    </div> 
</div>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#details" data-toggle="tab" aria-expanded="true">
            Detail </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active in" id="details">
        <p><strong>Name</strong>: {{ $manufacturer['name'] }}</p>
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Manufacturer {{ $manufacturer->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Manufacturers <small>Show Manufacturer: {{ $manufacturer->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\ManufacturersController@index') }}">Manufacturers</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Manufacturer {{ $manufacturer->name }}</a>
</li>
@stop
