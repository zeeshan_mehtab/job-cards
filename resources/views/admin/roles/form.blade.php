<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Name')) !!}
</div>

<div class="form-group {{ ($errors->has('display_name')) ? 'has-error' : '' }}">
    {!! Form::label('display_name', 'Display Name:') !!}
    {!! Form::text('display_name', null, array('class' => 'form-control', 'placeholder' => 'Display Name')) !!}
</div>

<div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, array('class' => 'form-control', 'placeholder' => 'Description')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>