@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $role['name'] }} Role</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('Auth\RolesController@edit', array($role->id)) }}'">Edit Role</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Role {{ $role->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Roles <small>Show Role: {{ $role->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\RolesController@index') }}">Roles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Role {{ $role->name }}</a>
</li>
@stop
