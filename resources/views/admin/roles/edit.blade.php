@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($role, ['action' =>  ['Auth\RolesController@update', $role->id], 'method' => 'patch']) !!}

        <h2>Edit Role <small>{{ $role->name }}</small></h2>

        @include('admin.roles.form', ['submitButtonText' => 'Update Role'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Role {{ $role->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Roles <small>Edit Role: {{ $role->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\RolesController@index') }}">Roles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Role {{ $role->name }}</a>
</li>
@stop