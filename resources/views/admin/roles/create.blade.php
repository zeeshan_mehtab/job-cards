@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Auth\RolesController@store')) !!}
        <h2>Create New Role</h2>

        @include('admin.roles.form', ['submitButtonText' => 'Add Role'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Role
@stop

{{-- Page Heading --}}
@section('heading')
Roles <small>Create New Role</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Auth\RolesController@index') }}">Roles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Role</a>
</li>
@stop