@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Auth\RolesController@create') }}'">
                            Add New Role <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($roles) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Role</th>
                    <th>Description</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                <tr>
                    <td><a href="{{ action('Auth\RolesController@show', array($role->id)) }}">{{ $role->display_name }}</a></td>
                    <td>{{ $role->description }}</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Auth\RolesController@edit', array($role->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Auth\RolesController@destroy', $role->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $roles->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Roles Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Roles <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Roles</a>
</li>
@stop