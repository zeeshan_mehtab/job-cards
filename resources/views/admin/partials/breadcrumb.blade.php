<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="/admin">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>
        @section('breadcrumb')

        @show
    </ul>
</div>