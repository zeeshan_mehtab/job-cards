<body data-layout-sidebar="fixed" data-layout-topbar="fixed">
    <div id="navigation">
        <div class="container-fluid">
            <a href="#" id="brand">Anwica</a>
            <a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation"><i class="icon-reorder"></i></a>
            <ul class='main-nav'>
                <li>
                    {{ HTML::link('admin/users', 'Dashboard') }}
                </li>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Users</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/users/index', 'Users List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/users/create', 'Create User') }}
                        </li>
                    </ul>
                </li>                               
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Locations</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/locations/index', 'Locations List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/locations/create', 'Create Location') }}
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Offers</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/offers/index', 'Offers List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/offers/create', 'Create Offer') }}
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Competitions</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/competitions/index', 'Competitions List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/competitions/create', 'Create Competition') }}
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Coupons</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/coupons/index', 'Coupons List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/coupons/create', 'Create Coupon') }}
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
                        <span>Scan Data</span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            {{ HTML::link('admin/scandatas/index', 'Data List') }}
                        </li>
                        <li>
                            {{ HTML::link('admin/scandatas/create', 'Create Scan Data') }}
                        </li>
                    </ul>
                </li>                                  
            </ul>
            <div class="user">
                <div class="dropdown">
                    <a href="#" class='dropdown-toggle' data-toggle="dropdown">{{ Auth::user()->first_name }} {{ HTML::image("img/demo/user-avatar.jpg", "") }}</a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="more-userprofile.html">Edit profile</a>
                        </li>
                        <li>
                            <a href="#">Account settings</a>
                        </li>
                        <li>
                            {{ HTML::link('account/logout', 'Sign Out') }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


</body>