<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <li class="sidebar-toggler-wrapper">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                </div>
                <!-- END SIDEBAR TOGGLER BUTTON -->
            </li>
            <li>
                <span>&nbsp;</span>
            </li>
            
            <li class="start {{ ( (Request::is('admin') || Request::is('*reports*')) ? 'active open' : '') }}">
                <a href="javascript:;">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="{{ (Request::is('admin') ? 'arrow open' : '') }} "></span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ (Request::is('admin') ? 'active' : '') }}">
                        <a href="/admin">
                            <i class="icon-bar-chart"></i>
                            Default Dashboard</a>
                    </li>
                    <li>
                        <a href="/admin/reports/totals-per-officer">
                            <i class="icon-briefcase"></i>
                            Totals Per Officer</a>
                    </li>
                    <li>
                        <a href="/admin/reports/commission-per-agent">
                            <i class="icon-briefcase"></i>
                            Commission Per Agent</a>
                    </li>
                </ul>
            </li>
            
            
            @if (Entrust::hasRole('super-admin') || Entrust::hasRole('admin'))
            <li class="{{ (Request::is('*setup*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Setup</span>
                    <span class="{{ (Request::is('*setup*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">
                    @if (Entrust::hasRole('super-admin'))
                    <li class="{{ (Request::is('*settings*') ? 'active' : '') }}">
                        <a href="/admin/setup/settings">
                            <i class="icon-settings"></i>
                            Settings</a>
                    </li>
                    @endif
                    
                    <li class="{{ (Request::is('*location_types*') ? 'active' : '') }}">
                        <a href="/admin/setup/location_types">
                            <i class="icon-settings"></i>
                            Location Types</a>
                    </li>
                    
                    <li class="{{ (Request::is('*services*') ? 'active' : '') }}">
                        <a href="/admin/setup/services">
                            <i class="icon-settings"></i>
                            Services</a>
                    </li>

                    <li class="{{ (Request::is('*categories*') ? 'active' : '') }}">
                        <a href="/admin/setup/categories">
                            <i class="icon-compass"></i>
                            Categories</a>
                    </li>

                    <li class="{{ (Request::is('*manufacturers*') ? 'active' : '') }}">
                        <a href="/admin/setup/manufacturers">
                            <i class="icon-compass"></i>
                            Manufacturers</a>
                    </li>
                </ul>
            </li>
            @endif

            @if (Entrust::hasRole('admin'))
            <li class="{{ (Request::is('*listing*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-list"></i>
                    <span class="title">Listing</span>
                    <span class="{{ (Request::is('*listing*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">

                    <li class="{{ (Request::is('*jobs*') ? 'active' : '') }}">
                        <a href="/admin/listing/jobs">
                            <i class="icon-list"></i>
                            Job Cards</a>
                    </li>
                    <li class="{{ (Request::is('*locations*') ? 'active' : '') }}">
                        <a href="/admin/listing/locations">
                            <i class="icon-list"></i>
                            locations</a>
                    </li>
                    <li class="{{ (Request::is('*vehicles*') ? 'active' : '') }}">
                        <a href="/admin/listing/vehicles">
                            <i class="icon-list"></i>
                            Vehicles</a>
                    </li>
                </ul>
            </li>
            @endif

            @if (Entrust::hasRole('super-admin'))
            <li class="{{ ( (Request::is('*auth*')) ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-user"></i>
                    <span class="title">User Management</span>
                    <span class="{{ ( (Request::is('*auth*') ) ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">
                    <li class="{{ (Request::is('*users*') ? 'active' : '') }}">
                        <a href="/admin/auth/users">
                            <i class="icon-user"></i>
                            Users</a>
                    </li>
                    @if (Auth::user()->id == 1))
                    <li class="{{ (Request::is('*groups*') ? 'active' : '') }}">
                        <a href="/admin/auth/roles">
                            <i class="icon-user"></i>
                            Roles</a>
                    </li>
                    @endif
                </ul>
            </li>
            @endif
            
            @if (Entrust::hasRole('driver'))
            <li class="{{ (Request::is('*listing*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-list"></i>
                    <span class="title">Listing</span>
                    <span class="{{ (Request::is('*listing*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">

                    <li class="{{ (Request::is('*jobs*') ? 'active' : '') }}">
                        <a href="/admin/listing/driver/jobs">
                            <i class="icon-list"></i>
                            Job Cards</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if (Entrust::hasRole('account'))
            <li class="{{ (Request::is('*listing*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-list"></i>
                    <span class="title">Listing</span>
                    <span class="{{ (Request::is('*listing*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">

                    <li class="{{ (Request::is('*jobs*') ? 'active' : '') }}">
                        <a href="/admin/listing/account/jobs">
                            <i class="icon-list"></i>
                            Job Cards</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if (Entrust::hasRole('agent'))
            <li class="{{ (Request::is('*listing*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-list"></i>
                    <span class="title">Listing</span>
                    <span class="{{ (Request::is('*listing*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">

                    <li class="{{ (Request::is('*jobs*') ? 'active' : '') }}">
                        <a href="/admin/listing/agent/jobs">
                            <i class="icon-list"></i>
                            Job Cards</a>
                    </li>
                </ul>
            </li>
            @endif
            
            @if (Entrust::hasRole('camp_boss'))
            <li class="{{ (Request::is('*listing*') ? 'active open' : '') }}" >
                <a href="javascript:;">
                    <i class="icon-list"></i>
                    <span class="title">Listing</span>
                    <span class="{{ (Request::is('*listing*') ? 'arrow open' : '') }}"></span>
                </a>
                <ul class="sub-menu">

                    <li class="{{ (Request::is('*jobs*') ? 'active' : '') }}">
                        <a href="/admin/listing/camp/jobs">
                            <i class="icon-list"></i>
                            Job Cards</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>