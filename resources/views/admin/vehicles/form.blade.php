<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ ($errors->has('identification_no')) ? 'has-error' : '' }}">
            {!! Form::label('identification_no', 'Identification No:') !!}
            {!! Form::text('identification_no', null, array('class' => 'form-control', 'placeholder' => 'Identification No')) !!}
        </div>

        <div class="form-group {{ ($errors->has('category_id')) ? 'has-error' : '' }}">
            {!! Form::label('category_id', 'Category:') !!}
            {!! Form::select('category_id', $categories, null, array('class' => 'form-control')) !!}
        </div>

        <div class="form-group {{ ($errors->has('manufacturer_id')) ? 'has-error' : '' }}">
            {!! Form::label('manufacturer_id', 'Manufacturer:') !!}
            {!! Form::select('manufacturer_id', $manufacturers, null, array('class' => 'form-control', 'id' => 'manufacturer_id')) !!}
        </div>

        <div class="form-group {{ ($errors->has('model_year')) ? 'has-error' : '' }}">
            {!! Form::label('model_year', 'Model Year:') !!}
            {!! Form::text('model_year', null, array('class' => 'form-control', 'placeholder' => 'Model year')) !!}
        </div>

    </div>    
    <div class="col-lg-6">

        <div class="form-group {{ ($errors->has('description')) ? 'has-error' : '' }}">
            {!! Form::label('description', 'Description:') !!}
            {!! Form::text('description', null, array('class' => 'form-control', 'placeholder' => 'Model Description')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('exterior_colour')) ? 'has-error' : '' }}">
            {!! Form::label('exterior_colour', 'Exterior Color:') !!}
            {!! Form::text('exterior_colour', null, array('class' => 'form-control', 'placeholder' => 'Exterior Color')) !!}
        </div>

        <div class="form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', ['available' => 'Available', 'sold' => 'Sold', 'removed' => 'Removed'],null, array('class' => 'form-control')) !!}
        </div>

    </div>
</div> 

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>

@section('pagelevel-scripts')


@stop