@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        {!! Form::model($vehicle, ['action' =>  ['Admin\VehiclesController@update', $vehicle->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

        <h2>Edit Vehicle <small>{{ $vehicle->identification_no }}</small></h2>

        @include('admin.vehicles.form', ['submitButtonText' => 'Update Vehicle'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Vehicle {{ $vehicle->identification_no }}
@stop

{{-- Page Heading --}}
@section('heading')
Vehicles <small>Edit Vehicle: {{ $vehicle->identification_no }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\VehiclesController@index') }}">Vehicles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Vehicle {{ $vehicle->identification_no }}</a>
</li>
@stop