{!! Form::model($filters, ['action' =>  ['Admin\VehiclesController@index'], 'method' => 'get', 'id' => 'filter-form']) !!}
<div class="row">
    <div class="table-toolbar">
        <div class="col-md-2">
            {!! Form::text('identification_no', null, array('class' => 'form-control', 'placeholder' => 'Identification No')) !!}
        </div>
        <div class="col-md-2">
            {!! Form::select('category', array('' => 'All Categories') + $all_categories, null, array('class' => 'form-control', 'id' => 'category_id')) !!}
        </div>
        <div class="col-md-2">
            {!! Form::select('manufacturer', array('' => 'All Manufacturers') + $all_manufacturers, null, array('class' => 'form-control', 'id' => 'manufacturer_id')) !!}
        </div>
        <div class="col-md-2">
            {!! Form::submit('Filter Data', array('class' => 'btn btn-primary')) !!}
        </div>
    </div>
</div>

@section('pagelevel-scripts')

<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.1/css/datepicker.css" rel="stylesheet" type="text/css"/>

<script>
    $('#date').datepicker({
        dateFormat: 'yy-mm-dd',
        inline: true,
    });
    
</script>

@stop

{!! Form::close() !!}