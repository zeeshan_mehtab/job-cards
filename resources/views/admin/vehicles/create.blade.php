@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        {!! Form::open(array('action' => 'Admin\VehiclesController@store', 'enctype' => 'multipart/form-data')) !!}
        <h2>Create New Vehicle</h2>

        @include('admin.vehicles.form', ['submitButtonText' => 'Add Vehicle'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Vehicle
@stop

{{-- Page Heading --}}
@section('heading')
Vehicles <small>Create New Vehicle</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\VehiclesController@index') }}">Vehicles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Vehicle</a>
</li>
@stop