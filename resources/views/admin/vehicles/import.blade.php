@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        {!! Form::open(array('action' => 'Admin\VehiclesController@post_import', 'enctype' => 'multipart/form-data' )) !!}
        <div class="form-group {{ ($errors->has('file')) ? 'has-error' : '' }}">
            {!! Form::label('file', 'File:') !!}
            {!! Form::file('file', null, array('class' => 'form-control', 'placeholder' => 'File')) !!}
        </div>
        <div class="form-group">
        {!! Form::submit('Import File', array('class' => 'btn btn-primary')) !!}
    </div>
        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Import Vehicles
@stop

{{-- Page Heading --}}
@section('heading')
Import Vehicles 
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\VehiclesController@index') }}">Vehicles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Import Vehicles</a>
</li>
@stop
