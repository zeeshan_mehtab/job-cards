@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

@include('admin.vehicles.filters')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\VehiclesController@create') }}'">
                            Add New Vehicle <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($vehicles) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Identification No</th>
                    <th>Make</th>
                    <th>Model Description</th>
                    <th>Vehicle Type</th>
                    <th>Model Year</th>
                    <th>Colour</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($vehicles as $vehicle)
                <tr>
                    <td><a href="{{ action('Admin\VehiclesController@show', array($vehicle->id)) }}">{{ $vehicle->identification_no }}</a></td>
                    <td>{{ $vehicle->manufacturer }}</td>
                    <td>{!! $vehicle->description !!}</td>
                    <td>{{ $vehicle->category }}</td>
                    <td>{{ $vehicle->model_year }}</td>
                    <td>{{ $vehicle->exterior_colour }}</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Admin\VehiclesController@edit', array($vehicle->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\VehiclesController@destroy', $vehicle->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $vehicles->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Vehicles Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Vehicles <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Vehicles</a>
</li>
@stop