@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="form-group">
    <button class="btn btn-primary" onClick="location.href ='{{ action('Admin\VehiclesController@edit', array($vehicle->id)) }}'">Edit Vehicle</button>
</div>    

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#info" data-toggle="tab" aria-expanded="true">
            Vehicle Information </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active in" id="info">
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Identification No</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $vehicle->identification_no }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Vehile Make (Manufacturer)</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $vehicle->manufacturer->name }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Vehicle Type (Category)</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $vehicle->category->name }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Model Year</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $vehicle->model_year }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Exterior Colour</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $vehicle->exterior_colour }}</p>
            </div>
        </div>
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Vehicle {{ $vehicle->identification_no }}
@stop

{{-- Page Heading --}}
@section('heading')
Vehicles <small>Show Vehicle: {{ $vehicle->identification_no }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\VehiclesController@index') }}">Vehicles</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">{{ $vehicle->identification_no }}</a>
</li>
@stop
