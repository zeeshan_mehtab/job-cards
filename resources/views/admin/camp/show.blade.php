@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="form-group">
    <button class="btn btn-primary" onClick="location.href ='{{ action('Admin\JobsController@edit', array($job->id)) }}'">Edit Job</button>
</div>    

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#info" data-toggle="tab" aria-expanded="true">
            Job Information </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active in" id="info">
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Customer Name</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->customer_name }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Customer Contact</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->customer_contact }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Service</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->service->title }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Agent</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->agent->name }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Driver</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->driver->name }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Pickup Location</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->pickup_location }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Pickup Address</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->pickup_address }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Dropoff Location</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->dropoff_location }}</p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-3 col-sm-4"><strong>Dropoff Address</strong></div>
            <div class="col-lg-9 col-sm-8">
                <p>{{ $job->dropoff_address }}</p>
            </div>
        </div>
        
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Job {{ $job->identification_no }}
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>Show Job: {{ $job->identification_no }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\JobsController@index') }}">Jobs</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">{{ $job->identification_no }}</a>
</li>
@stop
