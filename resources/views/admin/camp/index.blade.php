@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">

        @if(count($jobs) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Customer Contact</th>
                    <th>Service</th>
                    <th>Vehicle No</th>
                    <th>Pax</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobs as $job)
                <tr>
                    <td><a href="{{ action('Admin\CampsController@show', array($job->id)) }}">{{ $job->customer_name }}</a></td>
                    <td>{{ $job->customer_contact }}</td>
                    <td>{{ $job->service }}</td>
                    <td>
                        <p>
                        {{ $job->vehicle_plate_no}}
                        </p>
                    </td>
                    <td>
                        <p>
                            Pax: {{ $job->no_of_person }}
                        </p>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $jobs->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Jobs Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Jobs</a>
</li>
@stop