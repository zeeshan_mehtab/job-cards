@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\CategoriesController@create') }}'">
                            Add New Category <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($categories) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Category</th>
                    <th>Slug</th>
                    <th>Main Category</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                <tr>
                    <td><a href="{{ action('Admin\CategoriesController@show', array($category->id)) }}">{{ $category->name }}</a></td>
                    <td>{{ $category->slug }}</td>
                    <td>{{ $category->main_category }}</td>
                    <td class="text-right">
                        <button class="btn btn-default" onClick="location.href ='{{ action('Admin\CategoriesController@edit', array($category->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\CategoriesController@destroy', $category->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $categories->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Categories Found</div>
        @endif
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Categories <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Categories</a>
</li>
@stop