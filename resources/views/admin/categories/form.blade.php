<div class="form-group {{ ($errors->has('name')) ? 'has-error' : '' }}">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Name')) !!}
</div>

<div class="form-group {{ ($errors->has('slug')) ? 'has-error' : '' }}">
    {!! Form::label('slug', 'Slug:') !!}
    {!! Form::text('slug', null, array('class' => 'form-control', 'placeholder' => 'Slug')) !!}
</div>

<div class="form-group {{ ($errors->has('is_sellable')) ? 'has-error' : '' }}">
    {!! Form::label('main_category', 'Main Category:') !!}
    {!! Form::select('main_category', ['transport' => 'Transport', 'machinery' => 'Machinery'], null, array('class' => 'form-control')) !!}
</div>

<div class="form-group">
    {!! Form::submit($submitButtonText, array('class' => 'btn btn-primary')) !!}
</div>