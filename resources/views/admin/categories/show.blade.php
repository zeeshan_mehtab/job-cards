@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<h4>{{ $category['name'] }} Category</h4>
<div class="well clearfix">
    <div class="col-md-2">
        <button class="btn btn-primary" onClick="location.href ='{{ action('Admin\CategoriesController@edit', array($category->id)) }}'">Edit Category</button>
    </div> 
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Category {{ $category->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Categories <small>Show Category: {{ $category->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\CategoriesController@index') }}">Categories</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Show Category {{ $category->name }}</a>
</li>
@stop
