@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::open(array('action' => 'Admin\CategoriesController@store')) !!}
        <h2>Create New Category</h2>

        @include('admin.categories.form', ['submitButtonText' => 'Add Category'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Category
@stop

{{-- Page Heading --}}
@section('heading')
Categories <small>Create New Category</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\CategoriesController@index') }}">Categories</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Category</a>
</li>
@stop