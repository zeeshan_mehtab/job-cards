@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        {!! Form::model($category, ['action' =>  ['Admin\CategoriesController@update', $category->id], 'method' => 'patch']) !!}

        <h2>Edit Category <small>{{ $category->name }}</small></h2>

        @include('admin.categories.form', ['submitButtonText' => 'Update Category'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Edit Category {{ $category->name }}
@stop

{{-- Page Heading --}}
@section('heading')
Categories <small>Edit Category: {{ $category->name }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\CategoriesController@index') }}">Categories</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Edit Category {{ $category->name }}</a>
</li>
@stop