<div class="row">
    <div class="col-lg-4">
        <div class="form-group {{ ($errors->has('customer_name')) ? 'has-error' : '' }}">
            {!! Form::label('customer_name', 'Customer Name:') !!}
            {!! Form::text('customer_name', null, array('class' => 'form-control', 'placeholder' => 'Customer Name')) !!}
        </div>

        <div class="form-group {{ ($errors->has('customer_contact')) ? 'has-error' : '' }}">
            {!! Form::label('customer_contact', 'Customer Contact:') !!}
            {!! Form::text('customer_contact', null, array('class' => 'form-control', 'placeholder' => 'Customer Contact')) !!}
        </div>

        <div class="form-group {{ ($errors->has('no_of_person')) ? 'has-error' : '' }}">
            {!! Form::label('no_of_person', 'No of Person:') !!}
            {!! Form::number('no_of_person', null, array('class' => 'form-control', 'placeholder' => 'No of Person')) !!}
        </div>

        <div class="form-group {{ ($errors->has('no_of_person_charged')) ? 'has-error' : '' }}">
            {!! Form::label('no_of_person_charged', 'No of Person Charged:') !!}
            {!! Form::number('no_of_person_charged', null, array('id' => 'no_of_person_charged', 'class' => 'form-control', 'placeholder' => 'No of Person Charged')) !!}
        </div>

        <div class="form-group {{ ($errors->has('service_id')) ? 'has-error' : '' }}">
            {!! Form::label('service_id', 'Service:') !!}
            {!! Form::select('service_id', array('' => 'Select Service') + $all_services, null, array('class' => 'form-control')) !!}
        </div>

        <div class="form-group {{ ($errors->has('pickup_date')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_date', 'Pickup Date:') !!}
            {!! Form::text('pickup_date', null, array('id' => 'pickup_date' ,'class' => 'form-control', 'placeholder' => 'Pickup Date')) !!}
        </div>

        <div class="form-group {{ ($errors->has('pickup_time')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_time', 'Pickup Time:') !!}
            {!! Form::text('pickup_time', null, array('class' => 'form-control', 'placeholder' => 'Pickup Time')) !!}
        </div>
        
<!--        <div class="form-group {{ ($errors->has('start_date')) ? 'has-error' : '' }}">
            {!! Form::label('start_date', 'Start Date:') !!}
            {!! Form::text('start_date', null, array('id' => 'start_date' ,'class' => 'form-control', 'placeholder' => 'Start Date')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('end_date')) ? 'has-error' : '' }}">
            {!! Form::label('end_date', 'End Date:') !!}
            {!! Form::text('end_date', null, array('id' => 'end_date' ,'class' => 'form-control', 'placeholder' => 'End Date')) !!}
        </div>-->
        
    </div>
    <div class="col-lg-4">
        <div class="form-group {{ ($errors->has('pickup_location_id')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_location_id', 'Select Pickup Location or Specify Address:') !!}
            {!! Form::select('pickup_location_id', array('' => 'Select Location') + $all_locations, null, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group {{ ($errors->has('pickup_address')) ? 'has-error' : '' }}">
            {!! Form::label('pickup_address', 'Specify Address:') !!}
            {!! Form::text('pickup_address', null, array('class' => 'form-control', 'placeholder' => 'Pickup Address')) !!}
        </div>

        <div class="form-group {{ ($errors->has('dropoff_location_id')) ? 'has-error' : '' }}">
            {!! Form::label('dropoff_location_id', 'Select Dropoff Location or Specify Address:') !!}
            {!! Form::select('dropoff_location_id', array('' => 'Select Location') + $all_locations, null, array('class' => 'form-control')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('dropoff_address')) ? 'has-error' : '' }}">
            {!! Form::label('dropoff_address', 'Specify Address:') !!}
            {!! Form::text('dropoff_address', null, array('class' => 'form-control', 'placeholder' => 'Dropoff Address')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('comments')) ? 'has-error' : '' }}">
            {!! Form::label('comments', 'Extra Comments:') !!}
            {!! Form::text('comments', null, array('class' => 'form-control', 'placeholder' => 'Comments')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
            {!! Form::label('status', 'Status:') !!}
            {!! Form::select('status', ['initial' => 'Initial', 'pending' => 'Pending', 'cancelled' => 'Cancelled', 'approved' => 'Approved', 'finished' => 'Finished'],null, array('class' => 'form-control')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('payment_status')) ? 'has-error' : '' }}">
            {!! Form::label('payment_status', 'Payment Status:') !!}
            {!! Form::select('payment_status', ['pending' => 'Pending', 'received' => 'Received'],null, array('class' => 'form-control')) !!}
        </div>
        
    </div>
    <div class="col-lg-4">

        <div class="form-group {{ ($errors->has('agent_id')) ? 'has-error' : '' }}">
            {!! Form::label('agent_id', 'Agent:') !!}
            {!! Form::select('agent_id', array('' => 'Select Agent') + $all_agents, null, array('class' => 'form-control')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('rate')) ? 'has-error' : '' }}">
            {!! Form::label('rate', 'Rate:') !!}
            {!! Form::number('rate', null, array('id' => 'rate', 'class' => 'form-control', 'placeholder' => 'Rate')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('total_charges')) ? 'has-error' : '' }}">
            {!! Form::label('total_charges', 'Total Charges:') !!}
            {!! Form::number('total_charges', null, array('id' => 'total_charges', 'class' => 'form-control', 'placeholder' => 'Total Charges')) !!}
        </div>

        <div class="form-group {{ ($errors->has('commission')) ? 'has-error' : '' }}">
            {!! Form::label('commission', 'Commission Percentage:') !!}
            {!! Form::number('commission', null, array('id' => 'commission', 'class' => 'form-control', 'placeholder' => 'Commission Percentage')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('total_commission')) ? 'has-error' : '' }}">
            {!! Form::label('total_commission', 'Total Commission:') !!}
            {!! Form::number('total_commission', null, array('id' => 'total_commission', 'class' => 'form-control', 'placeholder' => 'Total Commission')) !!}
        </div>

        <div class="form-group {{ ($errors->has('driver_id')) ? 'has-error' : '' }}">
            {!! Form::label('driver_id', 'Driver:') !!}
            {!! Form::select('driver_id', array('' => 'Select Driver') + $all_drivers, null, array('class' => 'form-control', 'id' => 'driver_id')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('extra_drivers')) ? 'has-error' : '' }}">
            {!! Form::label('extra_drivers', 'Extra Drivers:') !!}
            {!! Form::text('extra_drivers', null, array('class' => 'form-control', 'placeholder' => 'Extra Drivers')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('camp_boss_id')) ? 'has-error' : '' }}">
            {!! Form::label('camp_boss_id', 'Camp Boss:') !!}
            {!! Form::select('camp_boss_id', array('1' => 'Select Camp Boss') + $all_camp_bosses, null, array('class' => 'form-control', 'id' => 'camp_boss_id')) !!}
        </div>
        
        <div class="form-group {{ ($errors->has('vehicle_plate_no')) ? 'has-error' : '' }}">
            {!! Form::label('vehicle_plate_no', 'Vehicle Plate No:') !!}
            {!! Form::text('vehicle_plate_no', null, array('class' => 'form-control', 'placeholder' => 'Vehicle Plate No')) !!}
        </div>

    </div>
</div> 

<div class="form-group">
	<button class="btn btn-primary" type="submit" name="action" value="save">Save</button>
	<button class="btn btn-primary" type="submit" name="action" value="save_and_close">Save &amp; Close</button>
</div>

@section('pagelevel-scripts')
    
<script src="/js/jquery-ui.min.js"></script>
<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/css/select2.min.css" rel="stylesheet" />
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1-rc.1/js/select2.min.js"></script>

<script>
$("#rate").on('change', function() {
    var number = $("#no_of_person_charged").val();
    var rate = $(this).val();
    var total = Math.ceil(number * rate);
    $("#total_charges").val(total);
    $( "#commission" ).trigger( "change" );
});

$("#commission").on('change', function() {
    var total = $("#total_charges").val();
    var commission_rate = $(this).val();
    var total_commission = Math.ceil((total / 100) * commission_rate);
    $("#total_commission").val(total_commission);
});
    
    
$('#pickup_date').datepicker({
    dateFormat: 'yy-mm-dd',
    inline: true,
});
$('#start_date').datepicker({
    dateFormat: 'yy-mm-dd',
    inline: true,
});
$('#end_date').datepicker({
    dateFormat: 'yy-mm-dd',
    inline: true,
});
$('select').select2();
</script>

@stop
