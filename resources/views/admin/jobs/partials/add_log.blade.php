<div class="row">
    <div class="col-md-12">
        <h2>Add Log</h2>
        {!! Form::open(array('action' => 'Admin\LogsController@store', 'enctype' => 'multipart/form-data')) !!}
        {!! Form::hidden('job_id', $job->id) !!}
        {!! Form::hidden('author_id', Auth::user()->id ) !!}
        <div class="form-group {{ ($errors->has('comments')) ? 'has-error' : '' }}">
            {!! Form::label('comments', 'Comments:') !!}
            {!! Form::textarea('comments', null, array('class' => 'form-control', 'placeholder' => 'comments')) !!}
        </div>

        
        <div class="form-group">
            {!! Form::submit('Add Log', array('class' => 'btn btn-primary')) !!}
        </div>

        {!! Form::close() !!}

    </div>
</div>