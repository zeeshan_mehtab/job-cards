@extends('admin.layouts.default')

{{-- Content --}}
@section('content')

@include('admin.jobs.filters')

<div class="row">
    <div class="col-md-12">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                        <button class="btn btn-primary" onClick="location.href ='{{ URL::action('Admin\JobsController@create') }}'">
                            Add New Job <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        @if(count($jobs) > 0)
        <table class="table table-striped table-hover table-bordered">
            <thead>
                <tr>
                    <th>Job #</th>
                    <th>Customer</th>
                    <th>Service</th>
                    <th>Location</th>
                    <th>Pax</th>
                    <th>Agent</th>
                    <th>Driver</th>
                    <th>Status</th>
                    <th>Total Charges</th>
                    <th class="text-right">Options</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($jobs as $job)
                <tr>
                    <td><a href="{{ action('Admin\JobsController@show', array($job->id)) }}">{{ $job->id }}</a></td>
                    <td><a href="{{ action('Admin\JobsController@show', array($job->id)) }}">{{ $job->customer_name }}</a><br/>
                    {{ $job->customer_contact }}
                    </td>
                    <td>{{ $job->service }}</td>
                    <td>
                        <p>
                            Pickup: @if($job->pickup_location) {{ $job->pickup_location }} <br> @endif
                            @if($job->pickup_address) {{ $job->pickup_address }} <br> @endif
                            Dropoff: @if($job->dropoff_location) {{ $job->dropoff_location }} <br> @endif
                            @if($job->dropoff_address) {{ $job->dropoff_address }} <br> @endif
                            Pickup Date: {{ $job->pickup_date }}<br>
                            Pickup Time: {{ $job->pickup_time }}<br>
                        </p>
                    </td>
                    <td>
                        <p>
                            Pax: {{ $job->no_of_person }}<br>
                            Pax Charged: {{ $job->no_of_person_charged }}<br>
                        </p>
                    </td>
                    <td>
                    @if($job->agent)    
                        {{ $job->agent->name }}
                    @endif
                    </td>
                    <td>
                    @if($job->driver)
                        {{ $job->driver->name }}
                    @endif
                    @if($job->extra_drivers)
                    <br/>
                    {{ $job->extra_drivers }}
                    @endif
                    </td>
                    <td><strong>Job:</strong> {{ ucwords($job->status) }}<br>
                        <strong>Payment:</strong> {{ ucwords($job->payment_status) }}
                    </td>
                    <td>AED {{ $job->total_charges }}</td>
                    <td class="text-right">
                        <button class="edit btn btn-default" onClick="location.href ='{{ action('Admin\JobsController@edit', array($job->id)) }}'">Edit</button>
                        {!! Form::open(['action' =>  ['Admin\JobsController@destroy', $job->id], 'method' => 'DELETE', 'class' => 'delete-form']) !!}
                        {!! Form::submit('Delete', array('class' => 'delete btn btn-danger')) !!}	
                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12 text-right">
                <?php echo $jobs->render(); ?>
            </div>
        </div>
        @else
        <div class="note note-warning">No Jobs Found</div>
        @endif

    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Home
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>listing</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="#">Jobs</a>
</li>
@stop