@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="form-group">
    <button class="btn btn-primary" onClick="location.href ='{{ action('Admin\JobsController@edit', array($job->id)) }}'">Edit Job</button>
</div>    

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#info" data-toggle="tab" aria-expanded="true">
            Job Information </a>
    </li>
    
    <li >
        <a href="#logs" data-toggle="tab" aria-expanded="true">
            Logs </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade active in" id="info">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="row">
                    <div class="col-sm-6"><strong>Customer Name</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->customer_name }}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6"><strong>Customer Contact</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->customer_contact }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>No of Person</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->no_of_person }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>No of Person Charged</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->no_of_person_charged }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Service</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->service->title }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Pickup Date</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->pickup_date }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Pickup Time</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->pickup_time }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
               
                <div class="row">
                    <div class="col-sm-6"><strong>Pickup Location</strong></div>
                    <div class="col-sm-6">
                        <p>@if($job->pickup_location) {{ $job->pickup_location->title }} @endif</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6"><strong>Pickup Address</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->pickup_address }}</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6"><strong>Dropoff Location</strong></div>
                    <div class="col-sm-6">
                        <p>@if($job->pickup_location) {{ $job->dropoff_location->title }} @endif</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6"><strong>Dropoff Address</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->dropoff_address }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Comments</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->comments }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Status</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->status }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Payment Status</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->payment_status }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                
                <div class="row">
                    <div class="col-sm-6"><strong>Agent</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->agent->name }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Rate</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->rate }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Total Charges</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->total_charges }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Commission</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->commission }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Total Commission</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->total_commission }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Driver</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->driver->name }}</p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-sm-6"><strong>Extra Driver</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->extra_driver }}</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6"><strong>Vehicle Plate No</strong></div>
                    <div class="col-sm-6">
                        <p>{{ $job->vehicle_plate_no }}</p>
                    </div>
                </div>
                
            </div>
        </div>
        
    </div>
    
    <div class="tab-pane fade in" id="logs">
        <div class="row">
            <div class="col col-lg-8">
                @if(count($job->logs) > 0)
                @foreach ($job->logs as $log)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            {{ $log->author->name }} @ <small>{{ $log->created_at }}</small>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            {{ $log->comments }}
                        </div>
                    </div>
                </div>
            @endforeach
        
            @else
                <div class="note note-warning">No logs found for this job</div>
            @endif
            </div>
            <div class="col col-lg-4">
                @include('admin.jobs.partials.add_log')
            </div>
        </div>
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Show Job {{ $job->id }}
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>Show Job: {{ $job->id }}</small>   
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\JobsController@index') }}">Jobs</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">{{ $job->id }}</a>
</li>
@stop
