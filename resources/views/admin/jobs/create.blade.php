@extends('admin.layouts.default')

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-md-12">
        {!! Form::open(array('action' => 'Admin\JobsController@store', 'enctype' => 'multipart/form-data')) !!}
        <h2>Create New Job</h2>

        @include('admin.jobs.form', ['submitButtonText' => 'Add Job'])

        {!! Form::close() !!}
    </div>
</div>
@stop

{{-- Web site Title --}}
@section('title')
@parent
Create Job
@stop

{{-- Page Heading --}}
@section('heading')
Jobs <small>Create New Job</small>
@stop

{{-- Page Breadcrumb --}}
@section('breadcrumb')
<li>
    <a href="{{ action('Admin\JobsController@index') }}">Jobs</a>
    <i class="fa fa-angle-right"></i>
</li>
<li>
    <a href="#">Create New Job</a>
</li>
@stop